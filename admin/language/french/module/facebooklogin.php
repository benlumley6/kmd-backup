<?php 
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6    				
// Created by villagedefrance                        	  		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading
$_['heading_title']       		= 'Facebook Login';

// Text
$_['text_module']         		= 'Modules';
$_['text_success']        		= 'Succ&#232;s: Vous avez modifi&#233; <b>Facebook Login</b> !';
$_['text_yes']	       			= 'Oui'; 
$_['text_no']	       			= 'Non';
$_['text_enable']	       		= 'Activ&#233;'; 
$_['text_disable']	       		= 'D&#233;sactiv&#233;';
$_['text_content_top']		= 'En-t&ecirc;te';
$_['text_content_bottom']	= 'Pied de page';
$_['text_column_left']			= 'Colonne de gauche';
$_['text_column_right']		= 'Colonne de droite';
$_['text_fblink_help']			= 'Obtenir votre Facebook API Key et votre API Secret code:';
$_['text_pwdsecret_help']	= 'Ajoute un cryptage suppl&#233;mentaire aux mots de passe : 4 &agrave; 8 caract&egrave;res minuscules (e.g: x2y4z6).';
$_['text_module_settings']	= 'Note: ces options seront appliqu&#233;es sur tous les modules dans la liste ci-dessous.';

// <H2>
$_['text_accountopt']   		= 'Options Comptes Clients';
$_['text_fbsettings']   		= 'Param&#232;tres Facebook';

// Tabs
$_['text_tabmain']        		= 'Options G&#233;n&#233;rales';
$_['text_tabaccount']        	= 'Options Comptes Clients';
$_['text_tabfacebook']       	= 'Param&#232;tres Facebook';

// Version Check
$_['text_module_name'] 		= 'Nom du Module : ';
$_['text_module_list']			= 'Compatibilit&#233; du Module : ';
$_['text_store_version'] 		= 'Version D&#233;tect&#233;e d&#8217;Opencart : <strong>%s</strong>';
$_['text_template']			= 'Th&egrave;me Actif : ';

$_['text_module_version'] 	= 'Version du Module : <strong>%s</strong>';
$_['text_module_revision'] 	= 'R&#233;vision du Module : <strong>%s</strong>';
$_['text_v_update'] 			= '<img src="view/image/warning.png" alt="" /> &nbsp; <strong>%s</strong> : Une Mise &agrave; Jour est disponible !';
$_['text_v_no_update'] 		= '<img src="view/image/success.png" alt="" /> &nbsp; <strong>%s</strong> : Vous avez la derni&egrave;re Version.';
$_['text_r_update'] 			= '<img src="view/image/warning.png" alt="" /> &nbsp; <strong>%s</strong> : Une Mise &agrave; Jour est disponible !';
$_['text_r_no_update'] 		= '<img src="view/image/success.png" alt="" /> &nbsp; <strong>%s</strong> : Vous avez la derni&egrave;re R&#233;vision.';
$_['text_no_revision'] 		= '<img src="view/image/warning.png" alt="" /> &nbsp; <b>R&#233;vision non-disponible.</b> Veuillez mettre &agrave; jour votre Version !';
$_['text_no_file'] 				= 'Le Fichier de V&#233;rification de Version n&#8217;est pas disponible sur le serveur !';
$_['text_status'] 				= '&Eacute;TAT ACTUEL DU MODULE';
$_['text_update'] 				= 'OPTIONS DE MISE &Agrave; JOUR';
$_['text_getupdate'] 			= 'Cliquez sur un des liens ci-dessous pour vous connecter &agrave; votre compte et t&#233;l&#233;charger la derni&egrave;re Mise &agrave; Jour.';

$_['button_showhide'] 		= 'V&#233;rificateur de Version';
$_['button_support'] 			= 'Assistance';

$_['text_v153']				= 'v1.5.3, ';
$_['text_v154']				= 'v1.5.4, ';
$_['text_v155']				= 'v1.5.5, ';
$_['text_v156']				= 'v1.5.6.';

// Entry
$_['entry_title']      			= 'Titre Alternatif:<br /><span class="help">Si laiss&#233; en blanc et "Montrer Le Titre" est s&#233;lectionn&#233;, la variable du langage sera utilis&#233;e.</span>'; 
$_['entry_header'] 			= 'Montrer Le Titre:<br /><span class="help">"Montrer Le Conteneur" doit &ecirc;tre s&#233;lectionn&#233;.</span>'; 
$_['entry_icon']   				= 'Montrer L&#8217;Ic&ocirc;ne:<br /><span class="help">"Montrer Le Conteneur" doit &ecirc;tre s&#233;lectionn&#233;.</span>'; 
$_['entry_box']   				= 'Montrer Le Conteneur:';

$_['entry_wishlist']   			= 'Cacher la Liste de Souhaits:'; 
$_['entry_download']   		= 'Cacher les T&#233;l&#233;chargements:'; 
$_['entry_reward']   			= 'Cacher les Points de Fidelit&eacute;:'; 
$_['entry_voucher']        		= 'Cacher les Ch&#232;ques-cadeaux:';
$_['entry_letter']        		= 'Cacher la Lettre d&#8217;Information:';

$_['entry_number']   			= 'Montrer l&#8217;ID du Client:'; 
$_['entry_balance']   			= 'Montrer la Balance du Compte:'; 
$_['entry_points']   			= 'Montrer le Total de Points:'; 

$_['entry_fblogin']        		= 'Activer Facebook Login:';
$_['entry_apikey']        		= 'Facebook API Key:<br /><span class="help">Requis si Facebook Login est Activ&#233;.</span>';
$_['entry_apisecret']     		= 'Facebook API Secret:<br /><span class="help">Requis si Facebook Login est Activ&#233;.</span>';
$_['entry_pwdsecret']     		= 'Code Mots de Passe:<br /><span class="help">Requis si Facebook Login est Activ&#233;.</span>';
$_['entry_button']	  			= 'Editer le Bouton Facebook:';

$_['entry_layout']        		= 'Disposition';
$_['entry_position']			= 'Emplacement';
$_['entry_status']				= '&Eacute;tat';
$_['entry_sort_order']			= 'Classement';

// Buttons
$_['button_save']				= 'Sauver';
$_['button_apply']				= 'Appliquer';

// Error
$_['error_permission']			= 'Attention, vous n&#8217;avez pas la permission de modifier le module <b>Facebook Login</b> !';
?>