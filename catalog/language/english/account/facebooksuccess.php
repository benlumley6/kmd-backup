<?php 
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6    			
// Created by villagedefrance                        	  
// contact@villagedefrance.net                         
//-----------------------------------------------------

// Heading
$_['heading_title'] 		= 'Your Account Has Been Created!';

// Text
$_['text_message']  		= '<p>Congratulations! Your new account has been successfully created using Facebook!</p> <p><h5>Important Account Information is required!</h5>Please check your new account details and kindly provide us with a valid <b>Shipping Address</b> and a <b>Contact Telephone Number</b> before placing your first order. Click <b>"Edit Account"</b> at the bottom to enter your account.</p> <p>If you have ANY questions about the operation of this online shop, please email the store owner.</p> <p>A confirmation has been sent to the provided email address. If you have not received it within the hour, please <a href="%s">contact us</a>.</p>';
$_['text_approval'] 		= '<p>Thank you for registering with %s!</p><p>You will be notified by email once your account has been activated by the store owner.</p><p>Please note that a valid <b>Shipping Address</b> and a <b>Contact Telephone Number</b> will be required initially before placing your first order.</p><p>If you have ANY questions about the operation of this online shop, please <a href="%s">contact the store owner</a>.</p>';
$_['text_account']  		= 'Account';
$_['text_success']  		= 'Success';

// Button
$_['button_editaccount']	= 'Edit Account';
$_['button_continue']  	= 'Continue Shopping';
?>