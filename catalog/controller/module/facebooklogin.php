<?php 
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6    				
// Created by villagedefrance                        	  		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

class ControllerModuleFacebookLogin extends Controller { 
	private $_name = 'facebooklogin';

	protected function index($setting) {
		static $module = 0;
	
		$this->language->load('module/' . $this->_name);
	
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['logging_text'] = $this->language->get('logging_text');
		$this->data['canclled_text'] = $this->language->get('canclled_text');
	
		$this->load->model('localisation/language');
	
		$this->data['title'] = $this->config->get($this->_name . '_title' . $this->config->get('config_language_id'));
		$this->data['header'] = $this->config->get($this->_name . '_header');
 
		if (!$this->data['title']) { $this->data['title'] = $this->data['heading_title']; } 
		if (!$this->data['header']) { $this->data['title'] = ''; }
	
		$this->data['icon'] = $this->config->get($this->_name . '_icon');
		$this->data['box'] = $this->config->get($this->_name . '_box');
	
		$this->data['showwishlist'] = $this->config->get($this->_name . '_wishlist');
		$this->data['showdownload'] = $this->config->get($this->_name . '_download');
		$this->data['showreward'] = $this->config->get($this->_name . '_reward');
		$this->data['showvoucher'] = $this->config->get($this->_name . '_voucher');
		$this->data['showletter'] = $this->config->get($this->_name . '_letter');
	
		$this->data['number'] = $this->config->get($this->_name . '_number');
		$this->data['credit'] = $this->config->get($this->_name . '_balance');
		$this->data['earned'] = $this->config->get($this->_name . '_points');
	
		$this->load->model('account/reward');
	
		if ($this->customer->isLogged()) {
			$this->data['text_greeting'] = sprintf($this->language->get('text_greeting'), $this->customer->getFirstName());
			$this->data['code'] = $this->customer->getId();
			$this->data['balance'] = $this->currency->format($this->customer->getBalance(), $this->currency->getCode());
			$this->data['points'] = sprintf($this->language->get('text_points'), $this->model_account_reward->getTotalPoints());
		}
	
		$this->data['code'] = $this->customer->getId();
	
		$this->data['text_register'] = $this->language->get('text_register');
		$this->data['text_login'] = $this->language->get('text_login');
		$this->data['text_logout'] = $this->language->get('text_logout');
		$this->data['text_forgotten'] = $this->language->get('text_forgotten');
		$this->data['text_account'] = $this->language->get('text_account');
		$this->data['text_edit'] = $this->language->get('text_edit');
		$this->data['text_password'] = $this->language->get('text_password');
		$this->data['text_address'] = $this->language->get('text_address');
		$this->data['text_wishlist'] = $this->language->get('text_wishlist');
		$this->data['text_order'] = $this->language->get('text_order');
		$this->data['text_transaction'] = $this->language->get('text_transaction');
		$this->data['text_download'] = $this->language->get('text_download');
		$this->data['text_addreturn'] = $this->language->get('text_addreturn');
		$this->data['text_return'] = $this->language->get('text_return');
		$this->data['text_reward'] = $this->language->get('text_reward');
		$this->data['text_giftvoucher'] = $this->language->get('text_giftvoucher');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
		$this->data['text_code'] = $this->language->get('text_code');
		$this->data['text_credit'] = $this->language->get('text_credit');
	
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_password'] = $this->language->get('entry_password');
	
		$this->data['button_login'] = $this->language->get('button_login');
		$this->data['button_logout'] = $this->language->get('button_logout');
	
		$this->data['logged'] = $this->customer->isLogged();
		$this->data['register'] = $this->url->link('account/register', '', 'SSL');
		$this->data['login'] = $this->url->link('account/login', '', 'SSL');
		$this->data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$this->data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');
		$this->data['account'] = $this->url->link('account/account', '', 'SSL');
		$this->data['edit'] = $this->url->link('account/edit', '', 'SSL');
		$this->data['password'] = $this->url->link('account/password', '', 'SSL');
		$this->data['address'] = $this->url->link('account/address', '', 'SSL');
		$this->data['wishlist'] = $this->url->link('account/wishlist');
		$this->data['order'] = $this->url->link('account/order', '', 'SSL');
		$this->data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$this->data['download'] = $this->url->link('account/download', '', 'SSL');
		$this->data['addreturn'] = $this->url->link('account/return/insert', '', 'SSL');
		$this->data['return'] = $this->url->link('account/return', '', 'SSL');
		$this->data['reward'] = $this->url->link('account/reward', '', 'SSL');
		$this->data['giftvoucher'] = $this->url->link('account/voucher', '', 'SSL');
		$this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
	
		$this->data['action'] = $this->url->link('account/login', '', 'SSL');
	
		// Position
		$this->data['position'] = $setting['position'];
	
		if ($this->data['position'] == 'column_left') {
			$this->data['loginformat'] = true;
		} elseif ($this->data['position'] == 'column_right') {
			$this->data['loginformat'] = true;
		} else {
			$this->data['loginformat'] = false;
		}
	
		// Facebook Login Start
		$this->data[$this->_name . '_fblogin'] = $this->config->get($this->_name . '_fblogin');
	
		$this->data['apikey'] = $this->config->get($this->_name . '_apikey');
		$this->data['apisecret'] = $this->config->get($this->_name . '_apisecret');
		$this->data['pwdsecret'] = $this->config->get($this->_name . '_pwdsecret');
	
		if ($this->data[$this->_name . '_fblogin'] == 'disable') {
			$this->data['fblogin'] = false;
		} elseif (!$this->data['apikey']) { 
			$this->data['fblogin'] = false; 
		} elseif (!$this->data['apisecret']) { 
			$this->data['fblogin'] = false; 
		} elseif (!$this->data['pwdsecret']) { 
			$this->data['fblogin'] = false; 
		} else {
			$this->data['fblogin'] = true;
		}
	
		if ((!$this->customer->isLogged()) && $this->data['fblogin']) {
		
			$this->document->addStyle('catalog/view/theme/default/stylesheet/fbbutton.css');
		
			if ($this->config->get($this->_name . '_button_' . $this->config->get('config_language_id'))) {
				$this->data[$this->_name . '_button'] = $this->config->get($this->_name . '_button_' . $this->config->get('config_language_id'));
			} else {
				$this->data[$this->_name . '_button'] = $this->language->get('heading_title');
			}
		
			$this->data['facebooklogin']['appid'] = $this->config->get($this->_name . '_apikey');
			$this->data['facebooklogin']['secret'] = $this->config->get($this->_name . '_apisecret');
			$this->data['facebooklogin']['scope'] = 'email,user_birthday,user_location,user_hometown';
			$this->data['facebooklogin']['redirect_uri'] = $this->url->link('account/facebooklogin', '', 'SSL');
		
			if (!isset($this->facebooklogin)) {			
				require_once(DIR_SYSTEM . 'vendor/facebook-sdk/facebook.php');
				$this->facebooklogin = new Facebook(array(
					'appId' => $this->data['facebooklogin']['appid'],
					'secret' => $this->data['facebooklogin']['secret']
				));
			}
		
			$this->data['facebooklogin_url'] = $this->facebooklogin->getLoginUrl(array(
				'scope' => $this->data['facebooklogin']['scope'],
				'redirect_uri'  => $this->data['facebooklogin']['redirect_uri']
			));
		}
	
		$this->data['appid'] = $this->config->get($this->_name . '_apikey');
		$this->data['scope'] = 'email,user_birthday,user_location,user_hometown';
		$this->data['redirect_uri'] = $this->url->link('account/facebooklogin', '', 'SSL');
		// Facebook Login End
	
		$this->data['module'] = $module++; 
	
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/' . $this->_name . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/' . $this->_name . '.tpl';
		} else {
			$this->template = 'default/template/module/' . $this->_name . '.tpl';
		}
	
		$this->render();
	}
}
?>