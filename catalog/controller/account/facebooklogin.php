<?php 
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6    				
// Created by villagedefrance                        	  		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

class ControllerAccountFacebookLogin extends Controller { 
	private $error = array();
	private $_name = 'facebooklogin';

  	public function index() {
	
		if ($this->customer->isLogged()) {
	  		$this->redirect($this->url->link('account/account', '', 'SSL'));
		}
	
		$this->language->load('module/' . $this->_name);
	
		if (!isset($this->facebooklogin)) {			
			require_once(DIR_SYSTEM . 'vendor/facebook-sdk/facebook.php');
		
			$this->facebooklogin = new Facebook(array(
				'appId'  => $this->config->get($this->_name . '_apikey'),
				'secret' => $this->config->get($this->_name . '_apisecret')
			));
		}
	
		$_SERVER_CLEANED = $_SERVER;
		$_SERVER = $this->clean_decode($_SERVER);
	
		$fbuser = $this->facebooklogin->getUser();
		$fbuser_profile = null;
	
		if ($fbuser) {
			try {
				$fbuser_profile = $this->facebooklogin->api('/me');
			} catch (FacebookApiException $e) {
				error_log($e);
				$fbuser = null;
			}
		}
	
		$_SERVER = $_SERVER_CLEANED;
	
		if ($fbuser_profile['id'] && $fbuser_profile['email']) {
		
			$this->load->model('account/customer');
		
			$email = $fbuser_profile['email'];
			$password = $this->get_password($fbuser_profile['id']);
		
			if ($this->customer->login($email, $password)) {
				unset($this->session->data['guest']);
			
				// Default Shipping Address
				$this->load->model('account/address');
			
				$address_info = $this->model_account_address->getAddress($this->customer->getAddressId());
			
				if ($address_info) {
					if ($this->config->get('config_tax_customer') == 'shipping') {
						$this->session->data['shipping_country_id'] = $address_info['country_id'];
						$this->session->data['shipping_zone_id'] = $address_info['zone_id'];
						$this->session->data['shipping_postcode'] = $address_info['postcode'];	
					}
				
					if ($this->config->get('config_tax_customer') == 'payment') {
						$this->session->data['payment_country_id'] = $address_info['country_id'];
						$this->session->data['payment_zone_id'] = $address_info['zone_id'];
					} 
				} else { 
					unset($this->session->data['shipping_country_id']);
					unset($this->session->data['shipping_zone_id']);
					unset($this->session->data['shipping_postcode']);
					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
				
					unset($this->session->data['payment_country_id']);
					unset($this->session->data['payment_zone_id']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
				}
			
				// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
				if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
					$this->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
				} else {
					$this->redirect($this->url->link('account/account', '', 'SSL')); 
				}
			}
		
			$email_query = $this->db->query("SELECT 'email' FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "'");
		
			if ($email_query->num_rows) {
				$this->model_account_customer->editPassword($email, $password);
			
				if ($this->customer->login($email, $password)) {
					$this->redirect($this->url->link('account/account', '', 'SSL')); 
				}
			
			} else { 
			
				$this->request->post['email'] = $email;
			
				$add_data=array();
				$add_data['email'] = $fbuser_profile['email'];
				$add_data['password'] = $password;
				$add_data['firstname'] = isset($fbuser_profile['first_name']) ? $fbuser_profile['first_name'] : '';
				$add_data['lastname'] = isset($fbuser_profile['last_name']) ? $fbuser_profile['last_name'] : '';
				$add_data['telephone'] = '';
				$add_data['fax'] = '';
				$add_data['company'] = '';
				$add_data['company_id'] = '';
				$add_data['tax_id'] = '';
				$add_data['address_1'] = '';
				$add_data['address_2'] = '';
				$add_data['city'] = '';
				$add_data['postcode'] = '';
				$add_data['country_id'] = 0;
				$add_data['zone_id'] = 0;
			
				$this->model_account_customer->addCustomer($add_data);
			
				if ($this->customer->login($email, $password)) {
				
					unset($this->session->data['guest']);
				
					// Default Shipping Address
					if ($this->config->get('config_tax_customer') == 'shipping') {
						$this->session->data['shipping_country_id'] = $add_data['country_id'];
						$this->session->data['shipping_zone_id'] = $add_data['zone_id'];
						$this->session->data['shipping_postcode'] = $add_data['postcode'];
					
						unset($this->session->data['shipping_method']);
						unset($this->session->data['shipping_methods']);
					}
				
					// Default Payment Address
					if ($this->config->get('config_tax_customer') == 'payment') {
						$this->session->data['payment_country_id'] = $add_data['country_id'];
						$this->session->data['payment_zone_id'] = $add_data['zone_id'];
					
						unset($this->session->data['payment_method']);
						unset($this->session->data['payment_methods']);
					}
				
					$this->redirect($this->url->link('account/facebooksuccess'));
				}
			}
		}
	
		$this->redirect($this->url->link('account/account', '', 'SSL'));
	}

	private function get_password($str) {
		$password = $this->config->get($this->_name . '_pwdsecret') ? $this->config->get($this->_name . '_pwdsecret') : 'fb';
		$password.=substr($this->config->get($this->_name . '_apisecret'),0,3).substr($str,0,3).substr($this->config->get($this->_name . '_apisecret'),-3).substr($str,-3);
		return strtolower($password);
	}

	private function clean_decode($data) {
		if (is_array($data)) {
	  		foreach ($data as $key => $value) {
				unset($data[$key]);
				$data[$this->clean_decode($key)] = $this->clean_decode($value);
	  		}
		} else { 
	  		$data = htmlspecialchars_decode($data, ENT_COMPAT);
		}
	
		return $data;
	}
}
?>