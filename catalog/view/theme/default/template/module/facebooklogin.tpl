<?php

//-----------------------------------------------------

// Facebook Login for Opencart v1.5.6    				

// Created by villagedefrance                        	  		

// contact@villagedefrance.net                         		

//-----------------------------------------------------

?>



<?php if($box) { ?>

<div class="box">

	<div class="box-heading">

		<?php if($icon) { ?>

			<?php if($logged) { ?>

				<div style="float: left; margin-right: 8px;"><img src="catalog/view/theme/default/image/icon_user_login.png" alt="" /></div>

			<?php } else { ?>

				<div style="float: left; margin-right: 8px;"><img src="catalog/view/theme/default/image/icon_user_logout.png" alt="" /></div>

			<?php } ?>

		<?php } ?>

		<?php if($title) { ?>

			<?php echo $title; ?>

		<?php } ?>

	</div>

	

	<div class="box-content" style="margin-bottom: 0px;">

	<?php if($loginformat) { ?>

		<?php if($logged) { ?>

			<div style="text-align: left;">

				<?php echo $text_greeting; ?>

			</div>

			<br />

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></div>

				<?php if(!$showwishlist) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></div>

				<?php } ?>

			<br />

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></div>

				<?php if(!$showdownload) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></div>

				<?php } ?>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $addreturn; ?>"><?php echo $text_addreturn; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div>

				<?php if(!$showreward) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></div>

				<?php } ?>

				<?php if(!$showvoucher) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $giftvoucher; ?>"><?php echo $text_giftvoucher; ?></a></div>

				<?php } ?>

			<br />

				<?php if(!$showletter) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></div>

				<br />

				<?php } ?>

			<?php if ($number) { ?>

				<div style="text-align:center; padding: 3px 0px; font-weight:bold; color:#444;">

					<?php echo $text_code; ?>&nbsp;<?php echo $code; ?>

				</div>

			<?php } ?>

			<?php if ($credit) { ?>

				<div style="text-align:center; padding: 3px 0px; font-weight:bold; color:#444;">

					<?php echo $text_credit; ?>&nbsp;<?php echo $balance; ?>

				</div>

			<?php } ?>

			<?php if ($earned) { ?>

				<div style="text-align:center; padding: 3px 0px; font-weight:bold; color:#444;">

					<?php echo $points; ?>

				</div>

			<?php } ?>

			<div style="text-align:center; padding: 10px 0px;">

				<a href="index.php?route=account/logout" class="button non_cart"><span><?php echo $button_logout; ?></span></a>

			</div>

			

		<?php } else { ?>

		

			<div style="text-align:left; padding:5px;">

				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="module_facebooklogin">

					<p>

					<?php echo $entry_email; ?><br />

					<input type="text" name="email" /><br />

					<?php echo $entry_password; ?><br />

					<input type="password" name="password" /><br />

					</p>

					<p style="text-align:left;">

						<a onclick="$('#module_facebooklogin').submit();" class="button non_cart"><span><?php echo $button_login; ?></span></a>

					</p>

				</form>

				<div style="margin-top:10px; text-align:center;">

					<div style="margin-bottom:3px;"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>

					<div style="margin-bottom:3px;"><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></div>

				</div>

				<?php if($fblogin) { ?>

					<div id="fb-root"></div>

					<div class="box box-fbconnect" id="login-btr" style="text-align:center;">

						<a class="box-fbconnect-a" href="javascript:void(0);" id="fb-login"><span><?php echo $facebooklogin_button; ?></span></a>

					</div>

					<form id="login-fb-waiting" style="display:none; height:35px;">

						<div style="margin-top:20px; text-align:center;">

							<img src="catalog/view/theme/default/image/fb_loader.gif" style="margin-right: 10px; vertical-align: middle;" alt="" /> <span id="login-fb-waiting-text"></span>

						</div>

					</form>

				<?php } ?>

			</div>

		<?php } ?>

		

	<?php } else { ?>

	

		<?php if($logged) { ?>

		<div style="width:100%; overflow:auto;">

			<div style="float:left; width:32%; text-align:center;">

				<div style="padding: 5px 0px; font-weight:bold; color:#444;">

					<?php echo $text_greeting; ?>

				</div>

				<?php if ($number) { ?>

					<div style="padding: 3px 0px; font-weight:bold; color:#444;">

						<?php echo $text_code; ?>&nbsp;<?php echo $code; ?>

					</div>

				<?php } ?>

				<?php if ($credit) { ?>

					<div style="padding: 3px 0px; font-weight:bold; color:#444;">

						<?php echo $text_credit; ?>&nbsp;<?php echo $balance; ?>

					</div>

				<?php } ?>

				<?php if ($earned) { ?>

					<div style="padding: 3px 0px; font-weight:bold; color:#444;">

						<?php echo $points; ?>

					</div>

				<?php } ?>

				<div style="text-align:center; padding: 5px 0px;">

					<a href="index.php?route=account/logout" class="button non_cart"><span><?php echo $button_logout; ?></span></a>

				</div>

			</div>

			<div style="float:left; width:32%; text-align:left;">

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></div>

				<?php if(!$showwishlist) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></div>

				<?php } ?>

			<br />	

				<?php if(!$showletter) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></div>

				<?php } ?>

			</div>

			<div style="float:left; width:32%; text-align:left;">

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></div>

				<?php if(!$showdownload) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></div>

				<?php } ?>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $addreturn; ?>"><?php echo $text_addreturn; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div>

				<?php if(!$showreward) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></div>

				<?php } ?>

				<?php if(!$showvoucher) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $giftvoucher; ?>"><?php echo $text_giftvoucher; ?></a></div>

				<?php } ?>

			</div>

		</div>

		

		<?php } else { ?>

		

		<div style="width:100%; overflow:auto;">

			<div style="float:left; width:45%; text-align:left; padding:5px;">

				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="module_facebooklogin">

					<p>

					<?php echo $entry_email; ?><br />

					<input type="text" name="email" class="large-field" /><br />

					<?php echo $entry_password; ?><br />

					<input type="password" name="password" /> &nbsp; 

					<a onclick="$('#module_facebooklogin').submit();" class="button non_cart"><span><?php echo $button_login; ?></span></a>

					</p>

				</form>

			</div>

			<div style="float:left; width:45%; text-align:left; padding:5px;">

				<div style="margin-top:10px; text-align:center;">

					<div style="margin-bottom:3px;"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>

					<div style="margin-bottom:3px;"><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></div>

				</div>

				<?php if($fblogin) { ?>

					<div id="fb-root"></div>

					<div class="box box-fbconnect" id="login-btr" style="text-align:center;">

						<a class="box-fbconnect-a" href="javascript:void(0);" id="fb-login"><span><?php echo $facebooklogin_button; ?></span></a>

					</div>

					<form id="login-fb-waiting" style="display:none; height:35px;">

						<div style="margin-top:20px; text-align:center;">

							<img src="catalog/view/theme/default/image/fb_loader.gif" style="margin-right: 10px; vertical-align: middle;" alt="" /> <span id="login-fb-waiting-text"></span>

						</div>

					</form>

				<?php } ?>

			</div>

		</div>

		<?php } ?>

	<?php } ?>

	</div>

</div>



<?php } else { ?>



	<div style="margin-bottom:10px;">

	<?php if($loginformat) { ?>

		<?php if($logged) { ?>

			<div style="text-align:left;">

				<?php echo $text_greeting; ?>

			</div>

			<br />

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></div>

				<?php if(!$showwishlist) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></div>

				<?php } ?>

			<br />

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></div>

				<?php if(!$showdownload) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></div>

				<?php } ?>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $addreturn; ?>"><?php echo $text_addreturn; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div>

				<?php if(!$showreward) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></div>

				<?php } ?>

				<?php if(!$showvoucher) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $giftvoucher; ?>"><?php echo $text_giftvoucher; ?></a></div>

				<?php } ?>

			<br />

				<?php if(!$showletter) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></div>

				<br />

				<?php } ?>

			<?php if ($number) { ?>

				<div style="text-align: center; padding: 3px 0px; font-weight: bold; color: #444;">

					<?php echo $text_code; ?>&nbsp;<?php echo $code; ?>

				</div>

			<?php } ?>

			<?php if ($credit) { ?>

				<div style="text-align: center; padding: 3px 0px; font-weight: bold; color: #444;">

					<?php echo $text_credit; ?>&nbsp;<?php echo $balance; ?>

				</div>

			<?php } ?>

			<?php if ($earned) { ?>

				<div style="text-align: center; padding: 3px 0px; font-weight: bold; color: #444;">

					<?php echo $points; ?>

				</div>

			<?php } ?>

			<div style="text-align:center; padding: 10px 0px;">

				<a href="index.php?route=account/logout" class="button  non_cart"><span><?php echo $button_logout; ?></span></a>

			</div>

			

		<?php } else { ?>

		

			<div style="text-align:left; padding:5px;">

				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="module_facebooklogin">

					<p>

					<?php echo $entry_email; ?><br />

					<input type="text" name="email" /><br />

					<?php echo $entry_password; ?><br />

					<input type="password" name="password" /><br />

					</p>

					<p style="text-align: center;">

						<a onclick="$('#module_facebooklogin').submit();" class="button non_cart"><span><?php echo $button_login; ?></span></a>

					</p>

				</form>

				<div style="margin-top:10px; text-align:center;">

					<div style="margin-bottom:3px;"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>

					<div style="margin-bottom:3px;"><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></div>

				</div>

				<?php if($fblogin) { ?>

					<div id="fb-root"></div>

					<div class="box box-fbconnect" id="login-btr" style="text-align:center;">

						<a class="box-fbconnect-a" href="javascript:void(0);" id="fb-login"><span><?php echo $facebooklogin_button; ?></span></a>

					</div>

					<form id="login-fb-waiting" style="display:none; height:35px;">

						<div style="margin-top:20px; text-align:center;">

							<img src="catalog/view/theme/default/image/fb_loader.gif" style="margin-right: 10px; vertical-align: middle;" alt="" /> <span id="login-fb-waiting-text"></span>

						</div>

					</form>

				<?php } ?>

			</div>

		<?php } ?>

		

	<?php } else { ?>

	

		<?php if($logged) { ?>

		<div style="width:100%; overflow:auto;">

			<div style="float:left; width:32%; text-align:center;">

				<div style="padding: 5px 0px; font-weight:bold; color:#444;">

					<?php echo $text_greeting; ?>

				</div>

				<?php if ($number) { ?>

					<div style="padding: 3px 0px; font-weight:bold; color:#444;">

						<?php echo $text_code; ?>&nbsp;<?php echo $code; ?>

					</div>

				<?php } ?>

				<?php if ($credit) { ?>

					<div style="padding: 3px 0px; font-weight:bold; color:#444;">

						<?php echo $text_credit; ?>&nbsp;<?php echo $balance; ?>

					</div>

				<?php } ?>

				<?php if ($earned) { ?>

					<div style="padding: 3px 0px; font-weight:bold; color:#444;">

						<?php echo $points; ?>

					</div>

				<?php } ?>

				<div style="text-align:center; padding: 5px 0px;">

					<a href="index.php?route=account/logout" class="button non_cart"><span><?php echo $button_logout; ?></span></a>

				</div>

			</div>

			<div style="float:left; width:32%; text-align:left;">

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></div>

				<?php if(!$showwishlist) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></div>

				<?php } ?>

			<br />	

				<?php if(!$showletter) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></div>

				<?php } ?>

			</div>

			<div style="float:left; width:32%; text-align:left;">

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></div>

				<?php if(!$showdownload) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></div>

				<?php } ?>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $addreturn; ?>"><?php echo $text_addreturn; ?></a></div>

				<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div>

				<?php if(!$showreward) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></div>

				<?php } ?>

				<?php if(!$showvoucher) { ?>

					<div style="text-decoration: none; padding: 1px 0px 1px 12px;"><a href="<?php echo $giftvoucher; ?>"><?php echo $text_giftvoucher; ?></a></div>

				<?php } ?>

			</div>

		</div>

		

		<?php } else { ?>

		

		<div style="width:100%; overflow:auto;">

			<div style="float:left; width:45%; text-align:left; padding:5px;">

				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="module_facebooklogin">

					<p>

					<?php echo $entry_email; ?><br />

					<input type="text" name="email" class="large-field" /><br />

					<?php echo $entry_password; ?><br />

					<input type="password" name="password" /> &nbsp; 

					<a onclick="$('#module_facebooklogin').submit();" class="button non_cart"><span><?php echo $button_login; ?></span></a>

					</p>

				</form>

			</div>

			<div style="float:left; width:45%; text-align:left; padding:5px;">

				<div style="margin-top:10px; text-align:center;">

					<div style="margin-bottom:3px;"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>

					<div style="margin-bottom:3px;"><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></div>

				</div>

				<?php if($fblogin) { ?>

					<div id="fb-root"></div>

					<div class="box box-fbconnect" id="login-btr" style="text-align:center;">

						<a class="box-fbconnect-a" href="javascript:void(0);" id="fb-login"><span><?php echo $facebooklogin_button; ?></span></a>

					</div>

					<form id="login-fb-waiting" style="display:none; height:35px;">

						<div style="margin-top:20px; text-align:center;">

							<img src="catalog/view/theme/default/image/fb_loader.gif" style="margin-right: 10px; vertical-align: middle;" alt="" /> <span id="login-fb-waiting-text"></span>

						</div>

					</form>

				<?php } ?>

			</div>

		</div>

		<?php } ?>

	<?php } ?>

	</div>

	

<?php } ?>



<script type="text/javascript"><!--

$('#module_facebooklogin input').keydown(function(e) {

	if (e.keyCode == 13) { $('#module_facebooklogin').submit(); }

});

//--></script>



<?php if($fblogin) { ?>

<script type="text/javascript"><!--

window.fbAsyncInit = function () {

	FB.init({

		appId: '<?php echo $appid; ?>',

		cookie: true,

		oauth: true

	});

	var fb_busy = false;

	$('#fb-login').click(function () {

		if (fb_busy) return;

		fb_busy = true;

		$('#login-fb-waiting-text').text('Connecting ...');

		$('#login-btr').hide('fade', 300);

		$('#login-fb-waiting').delay(300).show('fade', 250).delay(400);

		FB.login(function (res) {

			$('#login-fb-waiting').queue(function (next) {

				if (res.status == 'connected' && res.authResponse != null) {

					$('#login-fb-waiting-text').hide('fade', 250, function () {

						$(this).text('<?php echo $logging_text;?>').show('fade', 250).delay(400).queue(function (next2) {

							//$('#mode').val('facebook-auto');

							document.location.href='<?php echo $redirect_uri; ?>';

							//$('#login-btr').submit();

							next2();

						});

					});

				} else {

					$('#login-fb-waiting-text').hide('fade', 300, function () {

						$(this).text('<?php echo $canclled_text;?>').show('fade', 250).delay(400).queue(function (next2) {

							$('#login-fb-waiting').hide('fade', 300, function () {

								$('#login-btr').show('fade', 400, function () { fb_busy = false; });

							});

						next2();

						});

					});

				}

			next();

			});

		}, {

			scope:'<?php echo $scope; ?>'

		});

	});

};



$(function () {

	var e = document.createElement('script'); e.type = 'text/javascript'; e.async = true;

	e.src = 'https:' + '//connect.facebook.net/en_US/all.js';

	$('#fb-root').append(e);

});

//--></script>

<?php } ?>

