<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if (!empty($error['warning'])) { ?>
  <div class="warning"><?php echo $error['warning']; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
		    <td><?php echo $entry_limit_shown; ?></td>
			<td><input type="text" name="category_bestseller[limit_shown]" value="<?php if (isset($category_bestseller['limit_shown'])) echo $category_bestseller['limit_shown']; ?>" size="3" /></td>
		  </tr>
		  <tr>
		    <td><?php echo $entry_limit_hidden; ?></td>
			<td><input type="text" name="category_bestseller[limit_hidden]" value="<?php if (isset($category_bestseller['limit_hidden'])) echo $category_bestseller['limit_hidden']; ?>" size="3" /></td>
		  </tr>
		  <tr>
		    <td><?php echo $entry_image; ?></td>
			<td><input type="text" name="category_bestseller[image_width]" value="<?php if (isset($category_bestseller['image_width'])) echo $category_bestseller['image_width']; ?>" size="3" /> x
			  <input type="text" name="category_bestseller[image_height]" value="<?php if (isset($category_bestseller['image_height'])) echo $category_bestseller['image_height']; ?>" size="3" /></td>
		  </tr>
		  <tr>
		    <td><?php echo $entry_description; ?></td>
			<td><input type="text" name="category_bestseller[description]" value="<?php if (isset($category_bestseller['description'])) echo $category_bestseller['description']; ?>" size="3" /></td>
		  </tr>
		  <tr>
		    <td><?php echo $entry_sub_category; ?></td>
			<td><select name="category_bestseller[sub_category]">
			    <option value="0"<?php if (empty($category_bestseller['sub_category'])) echo ' selected="selected"'; ?>><?php echo $text_no; ?></option>
			    <option value="1"<?php if (!empty($category_bestseller['sub_category'])) echo ' selected="selected"'; ?>><?php echo $text_yes; ?></option>
			  </select></td>
		  </tr>
		  <tr>
		    <td><?php echo $entry_status; ?></td>
			<td><select name="category_bestseller[status]">
			    <option value="0"<?php if (empty($category_bestseller['status'])) echo ' selected="selected"'; ?>><?php echo $text_disabled; ?></option>
			    <option value="1"<?php if (!empty($category_bestseller['status'])) echo ' selected="selected"'; ?>><?php echo $text_enabled; ?></option>
			  </select></td>
		  </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>