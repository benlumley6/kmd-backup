<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <div class="checkout">
    <div id="checkout">
      <div class="checkout-heading"><?php echo $text_checkout_option; ?></div>
      <div class="checkout-content"></div>
    </div>
    <?php if (!$logged) { ?>
    <div id="payment-address">
      <div class="checkout-heading"><span><?php echo $text_checkout_account; ?></span></div>
      <div class="checkout-content"></div>
    </div>
    <?php } else { ?>
    <div id="payment-address">
      <div class="checkout-heading"><span><?php echo $text_checkout_payment_address; ?></span></div>
      <div class="checkout-content"></div>
    </div>
    <?php } ?>
    <?php if ($shipping_required) { ?>
    <div id="shipping-address">
      <div class="checkout-heading"><?php echo $text_checkout_shipping_address; ?></div>
      <div class="checkout-content"></div>
    </div>
    <div id="shipping-method">
      <div class="checkout-heading"><?php echo $text_checkout_shipping_method; ?></div>
      <div class="checkout-content"></div>
    </div>
    <?php } ?>
    <div id="payment-method">
      <div class="checkout-heading"><?php echo $text_checkout_payment_method; ?></div>
      <div class="checkout-content"></div>
    </div>
    <div id="confirm">
      <div class="checkout-heading"><?php echo $text_checkout_confirm; ?></div>
      <div class="checkout-content"></div>
    </div>
  </div>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('#checkout .checkout-content input[name=\'account\']').live('change', function() {
	if ($(this).attr('value') == 'register') {
		$('#payment-address .checkout-heading span').html('<?php echo $text_checkout_account; ?>');
	} else {
		$('#payment-address .checkout-heading span').html('<?php echo $text_checkout_payment_address; ?>');
	}
});

$('.checkout-heading a').live('click', function() {
	$('.checkout-content').slideUp('slow');
	
	$(this).parent().parent().find('.checkout-content').slideDown('slow');
});
<?php if (!$logged) { ?> 
$(document).ready(function() {
	$.ajax({
		url: 'index.php?route=checkout/login',
		dataType: 'html',
		success: function(html) {
			$('#checkout .checkout-content').html(html);
				
			$('#checkout .checkout-content').slideDown('slow');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});		
<?php } else { ?>
$(document).ready(function() {
	$.ajax({
		url: 'index.php?route=checkout/payment_address',
		dataType: 'html',
		success: function(html) {
			$('#payment-address .checkout-content').html(html);
			bongoMessageBilling();
			$('#payment-address select[name=\'country_id\']').change(bongoMessageBilling);
			$('#payment-address .checkout-content').slideDown('slow');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});
<?php } ?>

// Checkout
$('#button-account').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/' + $('input[name=\'account\']:checked').attr('value'),
		dataType: 'html',
		beforeSend: function() {
			$('#button-account').attr('disabled', true);
			$('#button-account').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},		
		complete: function() {
			$('#button-account').attr('disabled', false);
			$('.wait').remove();
		},			
		success: function(html) {
			$('.warning, .error').remove();
			
			$('#payment-address .checkout-content').html(html);
			bongoMessageBilling();
			$('#payment-address select[name=\'country_id\']').change(bongoMessageBilling);
			$('#checkout .checkout-content').slideUp('slow');
				
			$('#payment-address .checkout-content').slideDown('slow');
				
			$('.checkout-heading a').remove();
				
			$('#checkout .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

// Login
$('#button-login').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/login/validate',
		type: 'post',
		data: $('#checkout #login :input'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-login').attr('disabled', true);
			$('#button-login').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-login').attr('disabled', false);
			$('.wait').remove();
		},				
		success: function(json) {
			$('.warning, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			} else if (json['error']) {
				$('#checkout .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
				
				$('.warning').fadeIn('slow');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

// Register
$('#button-register').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/register/validate',
		type: 'post',
		data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-register').attr('disabled', true);
			$('#button-register').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-register').attr('disabled', false); 
			$('.wait').remove();
		},			
		success: function(json) {
			$('.warning, .error').remove();
						
			if (json['redirect']) {
				location = json['redirect'];				
			} else if (json['error']) {
				if (json['error']['warning']) {
					$('#payment-address .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
				}
				
				if (json['error']['firstname']) {
					$('#payment-address input[name=\'firstname\'] + br').after('<span class="error">' + json['error']['firstname'] + '</span>');
				}
				
				if (json['error']['lastname']) {
					$('#payment-address input[name=\'lastname\'] + br').after('<span class="error">' + json['error']['lastname'] + '</span>');
				}	
				
				if (json['error']['email']) {
					$('#payment-address input[name=\'email\'] + br').after('<span class="error">' + json['error']['email'] + '</span>');
				}
				
				if (json['error']['telephone']) {
					$('#payment-address input[name=\'telephone\'] + br').after('<span class="error">' + json['error']['telephone'] + '</span>');
				}	
					
				if (json['error']['company_id']) {
					$('#payment-address input[name=\'company_id\'] + br').after('<span class="error">' + json['error']['company_id'] + '</span>');
				}	
				
				if (json['error']['tax_id']) {
					$('#payment-address input[name=\'tax_id\'] + br').after('<span class="error">' + json['error']['tax_id'] + '</span>');
				}	
																		
				if (json['error']['address_1']) {
					$('#payment-address input[name=\'address_1\'] + br').after('<span class="error">' + json['error']['address_1'] + '</span>');
				}	
				
				if (json['error']['city']) {
					$('#payment-address input[name=\'city\'] + br').after('<span class="error">' + json['error']['city'] + '</span>');
				}	
				
				if (json['error']['postcode']) {
					$('#payment-address input[name=\'postcode\'] + br').after('<span class="error">' + json['error']['postcode'] + '</span>');
				}	
				
				if (json['error']['country']) {
					$('#payment-address select[name=\'country_id\'] + br').after('<span class="error">' + json['error']['country'] + '</span>');
				}	
				
				if (json['error']['zone']) {
					$('#payment-address select[name=\'zone_id\'] + br').after('<span class="error">' + json['error']['zone'] + '</span>');
				}
				
				if (json['error']['password']) {
					$('#payment-address input[name=\'password\'] + br').after('<span class="error">' + json['error']['password'] + '</span>');
				}	
				
				if (json['error']['confirm']) {
					$('#payment-address input[name=\'confirm\'] + br').after('<span class="error">' + json['error']['confirm'] + '</span>');
				}																																	
			} else {
                if (processBongoCheckoutBilling()) {
                    return -1;
                }
				<?php if ($shipping_required) { ?>				
				var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').attr('value');
				
				if (shipping_address) {
					$.ajax({
						url: 'index.php?route=checkout/shipping_method',
						dataType: 'html',
						success: function(html) {
							$('#shipping-method .checkout-content').html(html);
							
							$('#payment-address .checkout-content').slideUp('slow');
							
							$('#shipping-method .checkout-content').slideDown('slow');
							
							$('#checkout .checkout-heading a').remove();
							$('#payment-address .checkout-heading a').remove();
							$('#shipping-address .checkout-heading a').remove();
							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();											
							
							$('#shipping-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');									
							$('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	

							$.ajax({
								url: 'index.php?route=checkout/shipping_address',
								dataType: 'html',
								success: function(html) {
									$('#shipping-address .checkout-content').html(html);
									bongoMessageShipping();
									$('#shipping-address select[name=\'country_id\']').change(bongoMessageShipping);
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});	
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});	
				} else {
					$.ajax({
						url: 'index.php?route=checkout/shipping_address',
						dataType: 'html',
						success: function(html) {
							$('#shipping-address .checkout-content').html(html);
							bongoMessageShipping();
							$('#shipping-address select[name=\'country_id\']').change(bongoMessageShipping);
							$('#payment-address .checkout-content').slideUp('slow');
							
							$('#shipping-address .checkout-content').slideDown('slow');
							
							$('#checkout .checkout-heading a').remove();
							$('#payment-address .checkout-heading a').remove();
							$('#shipping-address .checkout-heading a').remove();
							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();							

							$('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});			
				}
				<?php } else { ?>
				$.ajax({
					url: 'index.php?route=checkout/payment_method',
					dataType: 'html',
					success: function(html) {
						$('#payment-method .checkout-content').html(html);
						
						$('#payment-address .checkout-content').slideUp('slow');
						
						$('#payment-method .checkout-content').slideDown('slow');
						
						$('#checkout .checkout-heading a').remove();
						$('#payment-address .checkout-heading a').remove();
						$('#payment-method .checkout-heading a').remove();								
						
						$('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});					
				<?php } ?>
				
				$.ajax({
					url: 'index.php?route=checkout/payment_address',
					dataType: 'html',
					success: function(html) {
						$('#payment-address .checkout-content').html(html);
						bongoMessageBilling();
						$('#payment-address select[name=\'country_id\']').change(bongoMessageBilling);
						$('#payment-address .checkout-heading span').html('<?php echo $text_checkout_payment_address; ?>');
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			}	 
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

// Payment Address	
$('#button-payment-address').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/payment_address/validate',
		type: 'post',
		data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-payment-address').attr('disabled', true);
			$('#button-payment-address').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-payment-address').attr('disabled', false);
			$('.wait').remove();
		},			
		success: function(json) {
			$('.warning, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			} else if (json['error']) {
				if (json['error']['warning']) {
					$('#payment-address .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
				}
								
				if (json['error']['firstname']) {
					$('#payment-address input[name=\'firstname\']').after('<span class="error">' + json['error']['firstname'] + '</span>');
				}
				
				if (json['error']['lastname']) {
					$('#payment-address input[name=\'lastname\']').after('<span class="error">' + json['error']['lastname'] + '</span>');
				}	
				
				if (json['error']['telephone']) {
					$('#payment-address input[name=\'telephone\']').after('<span class="error">' + json['error']['telephone'] + '</span>');
				}		
				
				if (json['error']['company_id']) {
					$('#payment-address input[name=\'company_id\']').after('<span class="error">' + json['error']['company_id'] + '</span>');
				}	
				
				if (json['error']['tax_id']) {
					$('#payment-address input[name=\'tax_id\']').after('<span class="error">' + json['error']['tax_id'] + '</span>');
				}	
														
				if (json['error']['address_1']) {
					$('#payment-address input[name=\'address_1\']').after('<span class="error">' + json['error']['address_1'] + '</span>');
				}	
				
				if (json['error']['city']) {
					$('#payment-address input[name=\'city\']').after('<span class="error">' + json['error']['city'] + '</span>');
				}	
				
				if (json['error']['postcode']) {
					$('#payment-address input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
				}	
				
				if (json['error']['country']) {
					$('#payment-address select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
				}	
				
				if (json['error']['zone']) {
					$('#payment-address select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
				}
			} else {
                if (processBongoCheckoutBilling()) {
                    return -1;
                }
				<?php if ($shipping_required) { ?>
				$.ajax({
					url: 'index.php?route=checkout/shipping_address',
					dataType: 'html',
					success: function(html) {
						$('#shipping-address .checkout-content').html(html);
						bongoMessageShipping();
						$('#shipping-address select[name=\'country_id\']').change(bongoMessageShipping);
						$('#payment-address .checkout-content').slideUp('slow');
						
						$('#shipping-address .checkout-content').slideDown('slow');
						
						$('#payment-address .checkout-heading a').remove();
						$('#shipping-address .checkout-heading a').remove();
						$('#shipping-method .checkout-heading a').remove();
						$('#payment-method .checkout-heading a').remove();
						
						$('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
				<?php } else { ?>
				$.ajax({
					url: 'index.php?route=checkout/payment_method',
					dataType: 'html',
					success: function(html) {
						$('#payment-method .checkout-content').html(html);
					
						$('#payment-address .checkout-content').slideUp('slow');
						
						$('#payment-method .checkout-content').slideDown('slow');
						
						$('#payment-address .checkout-heading a').remove();
						$('#payment-method .checkout-heading a').remove();
													
						$('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});	
				<?php } ?>
				
				$.ajax({
					url: 'index.php?route=checkout/payment_address',
					dataType: 'html',
					success: function(html) {
						$('#payment-address .checkout-content').html(html);
						bongoMessageBilling();
						$('#payment-address select[name=\'country_id\']').change(bongoMessageBilling);
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});					
			}	  
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

// Shipping Address			
$('#button-shipping-address').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/shipping_address/validate',
		type: 'post',
		data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'password\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address select'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-shipping-address').attr('disabled', true);
			$('#button-shipping-address').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-shipping-address').attr('disabled', false);
			$('.wait').remove();
		},			
		success: function(json) {
			$('.warning, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			} else if (json['error']) {
				if (json['error']['warning']) {
					$('#shipping-address .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
				}
								
				if (json['error']['firstname']) {
					$('#shipping-address input[name=\'firstname\']').after('<span class="error">' + json['error']['firstname'] + '</span>');
				}
				
				if (json['error']['lastname']) {
					$('#shipping-address input[name=\'lastname\']').after('<span class="error">' + json['error']['lastname'] + '</span>');
				}	
				
				if (json['error']['email']) {
					$('#shipping-address input[name=\'email\']').after('<span class="error">' + json['error']['email'] + '</span>');
				}
				
				if (json['error']['telephone']) {
					$('#shipping-address input[name=\'telephone\']').after('<span class="error">' + json['error']['telephone'] + '</span>');
				}		
										
				if (json['error']['address_1']) {
					$('#shipping-address input[name=\'address_1\']').after('<span class="error">' + json['error']['address_1'] + '</span>');
				}	
				
				if (json['error']['city']) {
					$('#shipping-address input[name=\'city\']').after('<span class="error">' + json['error']['city'] + '</span>');
				}	
				
				if (json['error']['postcode']) {
					$('#shipping-address input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
				}	
				
				if (json['error']['country']) {
					$('#shipping-address select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
				}	
				
				if (json['error']['zone']) {
					$('#shipping-address select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
				}
			} else {
			
				if (processBongoCheckoutShipping()) {
					return -1;
				}
				
				$.ajax({
					url: 'index.php?route=checkout/shipping_method',
					dataType: 'html',
					success: function(html) {
						$('#shipping-method .checkout-content').html(html);
						
						$('#shipping-address .checkout-content').slideUp('slow');
						
						$('#shipping-method .checkout-content').slideDown('slow');
						
						$('#shipping-address .checkout-heading a').remove();
						$('#shipping-method .checkout-heading a').remove();
						$('#payment-method .checkout-heading a').remove();
						
						$('#shipping-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');							
						
						$.ajax({
							url: 'index.php?route=checkout/shipping_address',
							dataType: 'html',
							success: function(html) {
								$('#shipping-address .checkout-content').html(html);
								bongoMessageShipping();
								$('#shipping-address select[name=\'country_id\']').change(bongoMessageShipping);
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});						
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});	
			}  
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

// Guest
$('#button-guest').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/guest/validate',
		type: 'post',
		data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address select'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-guest').attr('disabled', true);
			$('#button-guest').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-guest').attr('disabled', false); 
			$('.wait').remove();
		},			
		success: function(json) {
			$('.warning, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			} else if (json['error']) {
				if (json['error']['warning']) {
					$('#payment-address .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
				}
								
				if (json['error']['firstname']) {
					$('#payment-address input[name=\'firstname\'] + br').after('<span class="error">' + json['error']['firstname'] + '</span>');
				}
				
				if (json['error']['lastname']) {
					$('#payment-address input[name=\'lastname\'] + br').after('<span class="error">' + json['error']['lastname'] + '</span>');
				}	
				
				if (json['error']['email']) {
					$('#payment-address input[name=\'email\'] + br').after('<span class="error">' + json['error']['email'] + '</span>');
				}
				
				if (json['error']['telephone']) {
					$('#payment-address input[name=\'telephone\'] + br').after('<span class="error">' + json['error']['telephone'] + '</span>');
				}	
					
				if (json['error']['company_id']) {
					$('#payment-address input[name=\'company_id\'] + br').after('<span class="error">' + json['error']['company_id'] + '</span>');
				}	
				
				if (json['error']['tax_id']) {
					$('#payment-address input[name=\'tax_id\'] + br').after('<span class="error">' + json['error']['tax_id'] + '</span>');
				}	
																		
				if (json['error']['address_1']) {
					$('#payment-address input[name=\'address_1\'] + br').after('<span class="error">' + json['error']['address_1'] + '</span>');
				}	
				
				if (json['error']['city']) {
					$('#payment-address input[name=\'city\'] + br').after('<span class="error">' + json['error']['city'] + '</span>');
				}	
				
				if (json['error']['postcode']) {
					$('#payment-address input[name=\'postcode\'] + br').after('<span class="error">' + json['error']['postcode'] + '</span>');
				}	
				
				if (json['error']['country']) {
					$('#payment-address select[name=\'country_id\'] + br').after('<span class="error">' + json['error']['country'] + '</span>');
				}	
				
				if (json['error']['zone']) {
					$('#payment-address select[name=\'zone_id\'] + br').after('<span class="error">' + json['error']['zone'] + '</span>');
				}
			} else {
                if (processBongoCheckoutBilling()) {
                    return -1;
                }
				<?php if ($shipping_required) { ?>	
				var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').attr('value');
				
				if (shipping_address) {
					$.ajax({
						url: 'index.php?route=checkout/shipping_method',
						dataType: 'html',
						success: function(html) {
							$('#shipping-method .checkout-content').html(html);
							
							$('#payment-address .checkout-content').slideUp('slow');
							
							$('#shipping-method .checkout-content').slideDown('slow');
							
							$('#payment-address .checkout-heading a').remove();
							$('#shipping-address .checkout-heading a').remove();
							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();		
															
							$('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	
							$('#shipping-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');									
							
							$.ajax({
								url: 'index.php?route=checkout/guest_shipping',
								dataType: 'html',
								success: function(html) {
									$('#shipping-address .checkout-content').html(html);
									bongoMessageShipping();
									$('#shipping-address select[name=\'country_id\']').change(bongoMessageShipping);
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});					
				} else {
					$.ajax({
						url: 'index.php?route=checkout/guest_shipping',
						dataType: 'html',
						success: function(html) {
							$('#shipping-address .checkout-content').html(html);
							bongoMessageShipping();
							$('#shipping-address select[name=\'country_id\']').change(bongoMessageShipping);
							$('#payment-address .checkout-content').slideUp('slow');
							
							$('#shipping-address .checkout-content').slideDown('slow');
							
							$('#payment-address .checkout-heading a').remove();
							$('#shipping-address .checkout-heading a').remove();
							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();
							
							$('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
				<?php } else { ?>				
				$.ajax({
					url: 'index.php?route=checkout/payment_method',
					dataType: 'html',
					success: function(html) {
						$('#payment-method .checkout-content').html(html);
						
						$('#payment-address .checkout-content').slideUp('slow');
							
						$('#payment-method .checkout-content').slideDown('slow');
							
						$('#payment-address .checkout-heading a').remove();
						$('#payment-method .checkout-heading a').remove();
														
						$('#payment-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});				
				<?php } ?>
                
			}	 
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

// Guest Shipping
$('#button-guest-shipping').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/guest_shipping/validate',
		type: 'post',
		data: $('#shipping-address input[type=\'text\'], #shipping-address select'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-guest-shipping').attr('disabled', true);
			$('#button-guest-shipping').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-guest-shipping').attr('disabled', false); 
			$('.wait').remove();
		},			
		success: function(json) {
			$('.warning, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			} else if (json['error']) {
				if (json['error']['warning']) {
					$('#shipping-address .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
				}
								
				if (json['error']['firstname']) {
					$('#shipping-address input[name=\'firstname\']').after('<span class="error">' + json['error']['firstname'] + '</span>');
				}
				
				if (json['error']['lastname']) {
					$('#shipping-address input[name=\'lastname\']').after('<span class="error">' + json['error']['lastname'] + '</span>');
				}	
										
				if (json['error']['address_1']) {
					$('#shipping-address input[name=\'address_1\']').after('<span class="error">' + json['error']['address_1'] + '</span>');
				}	
				
				if (json['error']['city']) {
					$('#shipping-address input[name=\'city\']').after('<span class="error">' + json['error']['city'] + '</span>');
				}	
				
				if (json['error']['postcode']) {
					$('#shipping-address input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
				}	
				
				if (json['error']['country']) {
					$('#shipping-address select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
				}	
				
				if (json['error']['zone']) {
					$('#shipping-address select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
				}
			} else {
				if (processBongoCheckoutShipping()) {
					return -1;
				}
				$.ajax({
					url: 'index.php?route=checkout/shipping_method',
					dataType: 'html',
					success: function(html) {
						$('#shipping-method .checkout-content').html(html);
						
						$('#shipping-address .checkout-content').slideUp('slow');
						
						$('#shipping-method .checkout-content').slideDown('slow');
						
						$('#shipping-address .checkout-heading a').remove();
						$('#shipping-method .checkout-heading a').remove();
						$('#payment-method .checkout-heading a').remove();
							
						$('#shipping-address .checkout-heading').append('<a><?php echo $text_modify; ?></a>');
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});				
			}	 
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

$('#button-shipping-method').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/shipping_method/validate',
		type: 'post',
		data: $('#shipping-method input[type=\'radio\']:checked, #shipping-method textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-shipping-method').attr('disabled', true);
			$('#button-shipping-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-shipping-method').attr('disabled', false);
			$('.wait').remove();
		},			
		success: function(json) {
			$('.warning, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			} else if (json['error']) {
				if (json['error']['warning']) {
					$('#shipping-method .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
				}			
			} else {
				$.ajax({
					url: 'index.php?route=checkout/payment_method',
					dataType: 'html',
					success: function(html) {
						$('#payment-method .checkout-content').html(html);
						
						$('#shipping-method .checkout-content').slideUp('slow');
						
						$('#payment-method .checkout-content').slideDown('slow');

						$('#shipping-method .checkout-heading a').remove();
						$('#payment-method .checkout-heading a').remove();
						
						$('#shipping-method .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	

					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});					
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

$('#button-payment-method').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/payment_method/validate', 
		type: 'post',
		data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-payment-method').attr('disabled', true);
			$('#button-payment-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-payment-method').attr('disabled', false);
			$('.wait').remove();
		},			
		success: function(json) {
			$('.warning, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			} else if (json['error']) {
				if (json['error']['warning']) {
					$('#payment-method .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
				}			
			} else {
				$.ajax({
					url: 'index.php?route=checkout/confirm',
					dataType: 'html',
					success: function(html) {
						$('#confirm .checkout-content').html(html);
						
						$('#payment-method .checkout-content').slideUp('slow');
						
						$('#confirm .checkout-content').slideDown('slow');
						
						$('#payment-method .checkout-heading a').remove();
						
						$('#payment-method .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});					
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});
//--></script> 

<?php
    define('BONGO_CHECKOUT_PARTNER_KEY', 'f410edb705582174ecfea448214275dd');
    define('BONGO_CHECKOUT_URL', 'https://transaction.bongous.com/pay/92ef5/index.php');
    define('BONGO_CHECKOUT_SHIPPING_BY_PRODUCT', 0);
    define('BONGO_CHECKOUT_TOTAL_DOMESTIC_SHIPPING_CHARGE', 0);
?>
<form name="BongoCheckoutForm" id="BongoCheckoutForm" method="post" action="<?php echo BONGO_CHECKOUT_URL ?>">
    <input type="hidden" name="PARTNER_KEY" value="<?php echo BONGO_CHECKOUT_PARTNER_KEY ?>" />

    <input type="hidden" name="CUST_COUNTRY" value="" >
    <input type="hidden" name="CUST_FIRST_NAME" value="" >
    <input type="hidden" name="CUST_LAST_NAME" value="" >
    <input type="hidden" name="CUST_COMPANY" value="" >
    <input type="hidden" name="CUST_ADDRESS_LINE_1" value="" >
    <input type="hidden" name="CUST_ADDRESS_LINE_2" value="" >
    <input type="hidden" name="CUST_CITY" value="" >
    <input type="hidden" name="CUST_STATE" value="" >
    <input type="hidden" name="CUST_ZIP" value="" >
    <input type="hidden" name="CUST_PHONE" value="" >
    <input type="hidden" name="CUST_EMAIL" value="" >
	
	<input type="hidden" name="SHIP_COUNTRY" value="" >
    <input type="hidden" name="SHIP_FIRST_NAME" value="" >
    <input type="hidden" name="SHIP_LAST_NAME" value="" >
    <input type="hidden" name="SHIP_COMPANY" value="" >
    <input type="hidden" name="SHIP_ADDRESS_LINE_1" value="" >
    <input type="hidden" name="SHIP_ADDRESS_LINE_2" value="" >
    <input type="hidden" name="SHIP_CITY" value="" >
    <input type="hidden" name="SHIP_STATE" value="" >
    <input type="hidden" name="SHIP_ZIP" value="" >
    <input type="hidden" name="SHIP_PHONE" value="" >
    <input type="hidden" name="SHIP_EMAIL" value="" >
	
    <input type="hidden" name="TOTAL_DOMESTIC_SHIPPING_CHARGE"  value="<?php echo BONGO_CHECKOUT_TOTAL_DOMESTIC_SHIPPING_CHARGE ?>"  />
    <?php $contador = 0; ?>
    <?php foreach($products as $product): ?>
        <?php 
			$productName = $product['name'];
			
			$custom = array();
            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
					$custom[] = $option['name'] . ': ' . $option['option_value'];
                }
            }
		?>
        <input type="hidden" name="PRODUCT_ID_<?php echo $contador + 1 ?>" value="<?php echo htmlspecialchars($product['product_id']) ?>" />
        <input type="hidden" name="PRODUCT_NAME_<?php echo $contador + 1 ?>" value="<?php echo htmlspecialchars($productName) ?>" />
        <input type="hidden" name="PRODUCT_PRICE_<?php echo $contador + 1 ?>" value="<?php echo (float)$product['price'] ?>" />
        <input type="hidden" name="PRODUCT_Q_<?php echo $contador + 1 ?>" value="<?php echo (int)$product['quantity'] ?>" />
        <input type="hidden" name="PRODUCT_SHIPPING_<?php echo $contador + 1 ?>" value="<?php echo (float)BONGO_CHECKOUT_SHIPPING_BY_PRODUCT ?>" />
		<input type="hidden" name="PRODUCT_CUSTOM_1_<?php echo $contador + 1 ?>" value="<?php echo implode(', ', $custom) ?>" />
        <?php $contador++; ?>
    <?php endforeach ?>
</form>
<script type="text/javascript">
var countries = [];
            countries[0] = '';
            countries[1] = 'AF';
            countries[2] = 'AL';
            countries[3] = 'DZ';
            countries[4] = 'AS';
            countries[5] = 'AD';
            countries[6] = 'AO';
            countries[7] = 'AI';
            countries[8] = '';
            countries[9] = 'AG';
            countries[10] = 'AR';
            countries[11] = 'AM';
            countries[12] = 'AW';
            countries[13] = 'AU';
            countries[14] = 'AT';
            countries[15] = 'AZ';
            countries[16] = 'BS';
            countries[17] = 'BH';
            countries[18] = 'BD';
            countries[19] = 'BB';
            countries[20] = 'BY';
            countries[21] = 'BE';
            countries[22] = 'BZ';
            countries[23] = 'BJ';
            countries[24] = 'BM';
            countries[25] = 'BT';
            countries[26] = 'BO';
            countries[27] = 'BA';
            countries[28] = 'BW';
            countries[29] = '';
            countries[30] = 'BR';
            countries[31] = '';
            countries[32] = 'BN';
            countries[33] = 'BG';
            countries[34] = 'BF';
            countries[35] = 'BI';
            countries[36] = 'KH';
            countries[37] = 'CM';
            countries[38] = 'CA';
            countries[39] = 'CV';
            countries[40] = 'KY';
            countries[41] = 'CF';
            countries[42] = 'TD';
            countries[43] = 'CL';
            countries[44] = 'CN';
            countries[45] = '';
            countries[46] = '';
            countries[47] = 'CO';
            countries[48] = 'KM';
            countries[49] = 'CG';
            countries[50] = 'CK';
            countries[51] = 'CR';
            countries[52] = '';
            countries[53] = 'KR';
            countries[54] = '';
            countries[55] = 'CY';
            countries[56] = 'CZ';
            countries[57] = 'DK';
            countries[58] = 'DJ';
            countries[59] = 'DM';
            countries[60] = 'DO';
            countries[61] = 'TP';
            countries[62] = 'EC';
            countries[63] = 'EG';
            countries[64] = 'SV';
            countries[65] = 'GQ';
            countries[66] = 'ER';
            countries[67] = 'EE';
            countries[68] = 'ET';
            countries[69] = 'FK';
            countries[70] = 'FO';
            countries[71] = 'FJ';
            countries[72] = 'FI';
            countries[73] = 'FR';
            countries[74] = '';
            countries[75] = 'GF';
            countries[76] = '';
            countries[77] = '';
            countries[78] = 'GA';
            countries[79] = 'GM';
            countries[80] = 'GE';
            countries[81] = 'DE';
            countries[82] = 'GH';
            countries[83] = 'GI';
            countries[84] = 'GR';
            countries[85] = 'GL';
            countries[86] = 'GD';
            countries[87] = 'GP';
            countries[88] = 'GU';
            countries[89] = 'GT';
            countries[90] = 'GN';
            countries[91] = 'GW';
            countries[92] = 'GY';
            countries[93] = 'HT';
            countries[94] = '';
            countries[95] = 'HN';
            countries[96] = 'HK';
            countries[97] = 'HU';
            countries[98] = 'IS';
            countries[99] = 'IN';
            countries[100] = 'ID';
            countries[101] = 'IR';
            countries[102] = 'IQ';
            countries[103] = 'IE';
            countries[104] = 'IL';
            countries[105] = 'IT';
            countries[106] = 'JM';
            countries[107] = 'JP';
            countries[108] = 'JO';
            countries[109] = 'KZ';
            countries[110] = 'KE';
            countries[111] = 'KI';
            countries[112] = 'KR';
            countries[113] = '';
            countries[114] = 'KW';
            countries[115] = 'KG';
            countries[116] = 'LA';
            countries[117] = 'LV';
            countries[118] = 'LB';
            countries[119] = 'LS';
            countries[120] = 'LR';
            countries[121] = 'LY';
            countries[122] = 'LI';
            countries[123] = 'LT';
            countries[124] = 'LU';
            countries[125] = 'MO';
            countries[126] = '';
            countries[127] = 'MG';
            countries[128] = 'MW';
            countries[129] = 'MY';
            countries[130] = 'MV';
            countries[131] = 'ML';
            countries[132] = 'MT';
            countries[133] = 'MH';
            countries[134] = 'MQ';
            countries[135] = 'MR';
            countries[136] = 'MU';
            countries[137] = 'YT';
            countries[138] = 'MX';
            countries[139] = '';
            countries[140] = 'MD';
            countries[141] = 'MC';
            countries[142] = 'MN';
            countries[143] = 'MS';
            countries[144] = 'MA';
            countries[145] = 'MZ';
            countries[146] = 'MM';
            countries[147] = 'NA';
            countries[148] = 'NR';
            countries[149] = 'NP';
            countries[150] = 'NL';
            countries[151] = '';
            countries[152] = 'NC';
            countries[153] = 'NZ';
            countries[154] = 'NI';
            countries[155] = 'NE';
            countries[156] = 'NG';
            countries[157] = 'NU';
            countries[158] = '';
            countries[159] = '';
            countries[160] = 'NO';
            countries[161] = 'OM';
            countries[162] = 'PK';
            countries[163] = 'PW';
            countries[164] = 'PA';
            countries[165] = 'PG';
            countries[166] = 'PY';
            countries[167] = 'PE';
            countries[168] = 'PH';
            countries[169] = '';
            countries[170] = 'PL';
            countries[171] = 'PT';
            countries[172] = 'PR';
            countries[173] = 'QA';
            countries[174] = 'RE';
            countries[175] = 'RO';
            countries[176] = 'RU';
            countries[177] = 'RW';
            countries[178] = '';
            countries[179] = '';
            countries[180] = '';
            countries[181] = 'SM';
            countries[182] = 'ST';
            countries[183] = 'SA';
            countries[184] = 'SN';
            countries[185] = '';
            countries[186] = 'SC';
            countries[187] = 'SL';
            countries[188] = 'SG';
            countries[189] = 'SK';
            countries[190] = 'SI';
            countries[191] = 'SB';
            countries[192] = 'SO';
            countries[193] = 'ZA';
            countries[194] = '';
            countries[195] = 'ES';
            countries[196] = 'LK';
            countries[197] = '';
            countries[198] = '';
            countries[199] = '';
            countries[200] = 'SR';
            countries[201] = '';
            countries[202] = 'SZ';
            countries[203] = 'SE';
            countries[204] = 'CH';
            countries[205] = 'SY';
            countries[206] = 'TW';
            countries[207] = 'TJ';
            countries[208] = 'TZ';
            countries[209] = 'TH';
            countries[210] = 'TG';
            countries[211] = '';
            countries[212] = 'TO';
            countries[213] = 'TT';
            countries[214] = 'TN';
            countries[215] = 'TR';
            countries[216] = 'TM';
            countries[217] = 'TC';
            countries[218] = 'TV';
            countries[219] = 'UG';
            countries[220] = 'UA';
            countries[221] = 'AE';
            countries[222] = 'GB';
            countries[223] = 'US';
            countries[224] = 'US';
            countries[225] = 'UY';
            countries[226] = 'UZ';
            countries[227] = 'VU';
            countries[228] = '';
            countries[229] = 'VE';
            countries[230] = 'VN';
            countries[231] = 'VG';
            countries[232] = 'VI';
            countries[233] = '';
            countries[234] = 'WS';
            countries[235] = 'YE';
            countries[236] = 'YU';
            countries[237] = 'CD';
            countries[238] = 'ZM';
            countries[239] = 'ZW';
            
	var messageBongo = '<div class="divBI" >'	
		+ '<div style="background: none no-repeat scroll 5px 8px #E8FFDF;color: #555555;font-size: 11px;margin-bottom: 10px;padding: 8px 6px 8px 28px;">Because you have selected an <b>international address</b>, you are now being routed to <span style="font-weight:bold;color:#FF8C00">Bongo&nbsp;</span><span style="font-weight:bold;color:#32CD32">International</span>, our third party international payment and shipping partner. Shipping, duties, and taxes will be calculated on the next page.</div>'
		+ '</div>';		
	
	
	var bongoMessageBilling = function () {
		$('.divBI').remove();
		if ($('#payment-address select[name=\'country_id\']').val() != 223 && $('#shipping').attr('checked') == 'checked') {
			$('#shipping').parent().after(messageBongo);
		}
		
		$('#shipping').click(function () {
			bongoMessageBilling();
		});
	}
	
	var bongoMessageShipping = function () {
		$('.divBI').remove();
		if ($('#shipping-address select[name=\'country_id\']').val() != 223) {
			$('#shipping-address .checkout-content .buttons').before(messageBongo);
		}
	}
	
    var processBongoCheckoutBilling = function () {

        if ($('#payment-address select[name=\'country_id\']').val() != 223 && $('#shipping').attr('checked') == 'checked') {
            
            var country = '';
            if (countries[$('#payment-address select[name=\'country_id\']').val()]) {
                country = countries[$('#payment-address select[name=\'country_id\']').val()];
            }
            document.forms["BongoCheckoutForm"].CUST_COUNTRY.value          = country;
            document.forms["BongoCheckoutForm"].CUST_FIRST_NAME.value       = $('#payment-address input[name=\'firstname\']').val();
            document.forms["BongoCheckoutForm"].CUST_LAST_NAME.value        = $('#payment-address input[name=\'lastname\']').val();
            document.forms["BongoCheckoutForm"].CUST_COMPANY.value          = $('#payment-address input[name=\'company\']').val();
            document.forms["BongoCheckoutForm"].CUST_ADDRESS_LINE_1.value   = $('#payment-address input[name=\'address_1\']').val();
            document.forms["BongoCheckoutForm"].CUST_ADDRESS_LINE_2.value   = $('#payment-address input[name=\'address_2\']').val();
            document.forms["BongoCheckoutForm"].CUST_CITY.value             = $('#payment-address input[name=\'city\']').val();
            document.forms["BongoCheckoutForm"].CUST_STATE.value            = $('#payment-address select[name=\'zone_id\'] option:selected').text();
            document.forms["BongoCheckoutForm"].CUST_ZIP.value              = $('#payment-address input[name=\'postcode\']').val();
            document.forms["BongoCheckoutForm"].CUST_PHONE.value            = $('#payment-address input[name=\'telephone\']').val();
            document.forms["BongoCheckoutForm"].CUST_EMAIL.value            = $('#payment-address input[name=\'email\']').val();
			
			document.forms["BongoCheckoutForm"].SHIP_COUNTRY.value          = '';
            document.forms["BongoCheckoutForm"].SHIP_FIRST_NAME.value       = '';
            document.forms["BongoCheckoutForm"].SHIP_LAST_NAME.value        = '';
            document.forms["BongoCheckoutForm"].SHIP_COMPANY.value          = '';
            document.forms["BongoCheckoutForm"].SHIP_ADDRESS_LINE_1.value   = '';
            document.forms["BongoCheckoutForm"].SHIP_ADDRESS_LINE_2.value   = '';
            document.forms["BongoCheckoutForm"].SHIP_CITY.value             = '';
            document.forms["BongoCheckoutForm"].SHIP_STATE.value            = '';
            document.forms["BongoCheckoutForm"].SHIP_ZIP.value              = '';
            document.forms["BongoCheckoutForm"].SHIP_PHONE.value            = '';
            document.forms["BongoCheckoutForm"].SHIP_EMAIL.value            = '';
			
            document.getElementById('BongoCheckoutForm').submit();
            return true;
        }
        return false;
    }
	
	var processBongoCheckoutShipping = function () {

        if ($('#shipping-address select[name=\'country_id\']').val() != 223) {
            var country = '';
            if (countries[$('#payment-address select[name=\'country_id\']').val()]) {
                country = countries[$('#payment-address select[name=\'country_id\']').val()];
            }
			
            document.forms["BongoCheckoutForm"].CUST_COUNTRY.value          = country;
            document.forms["BongoCheckoutForm"].CUST_FIRST_NAME.value       = $('#payment-address input[name=\'firstname\']').val();
            document.forms["BongoCheckoutForm"].CUST_LAST_NAME.value        = $('#payment-address input[name=\'lastname\']').val();
            document.forms["BongoCheckoutForm"].CUST_COMPANY.value          = $('#payment-address input[name=\'company\']').val();
            document.forms["BongoCheckoutForm"].CUST_ADDRESS_LINE_1.value   = $('#payment-address input[name=\'address_1\']').val();
            document.forms["BongoCheckoutForm"].CUST_ADDRESS_LINE_2.value   = $('#payment-address input[name=\'address_2\']').val();
            document.forms["BongoCheckoutForm"].CUST_CITY.value             = $('#payment-address input[name=\'city\']').val();
            document.forms["BongoCheckoutForm"].CUST_STATE.value            = $('#payment-address select[name=\'zone_id\'] option:selected').text();
            document.forms["BongoCheckoutForm"].CUST_ZIP.value              = $('#payment-address input[name=\'postcode\']').val();
            document.forms["BongoCheckoutForm"].CUST_PHONE.value            = $('#payment-address input[name=\'telephone\']').val();
            document.forms["BongoCheckoutForm"].CUST_EMAIL.value            = $('#payment-address input[name=\'email\']').val();
			
			var countryShipping = '';
            if (countries[$('#shipping-address select[name=\'country_id\']').val()]) {
                countryShipping = countries[$('#shipping-address select[name=\'country_id\']').val()];
            }
			
			document.forms["BongoCheckoutForm"].SHIP_COUNTRY.value          = countryShipping;
            document.forms["BongoCheckoutForm"].SHIP_FIRST_NAME.value       = $('#shipping-address input[name=\'firstname\']').val();
            document.forms["BongoCheckoutForm"].SHIP_LAST_NAME.value        = $('#shipping-address input[name=\'lastname\']').val();
            document.forms["BongoCheckoutForm"].SHIP_COMPANY.value          = $('#shipping-address input[name=\'company\']').val();
            document.forms["BongoCheckoutForm"].SHIP_ADDRESS_LINE_1.value   = $('#shipping-address input[name=\'address_1\']').val();
            document.forms["BongoCheckoutForm"].SHIP_ADDRESS_LINE_2.value   = $('#shipping-address input[name=\'address_2\']').val();
            document.forms["BongoCheckoutForm"].SHIP_CITY.value             = $('#shipping-address input[name=\'city\']').val();
            document.forms["BongoCheckoutForm"].SHIP_STATE.value            = $('#shipping-address select[name=\'zone_id\'] option:selected').text();
            document.forms["BongoCheckoutForm"].SHIP_ZIP.value              = $('#shipping-address input[name=\'postcode\']').val();
            document.forms["BongoCheckoutForm"].SHIP_PHONE.value            = $('#payment-address input[name=\'telephone\']').val();
            document.forms["BongoCheckoutForm"].SHIP_EMAIL.value            = $('#payment-address input[name=\'email\']').val();
			
            document.getElementById('BongoCheckoutForm').submit();
            return true;
        }
        return false;
    }
</script>

<?php echo $footer; ?>