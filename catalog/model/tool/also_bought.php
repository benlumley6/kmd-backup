<?php
class ModelToolAlsoBought extends Model {
    private function products($id_list, $limit) {
    	foreach($id_list as $k => &$id) {
    		$id = (int) $id;
    		if(!$id) unset($id_list[$k]);
    	}
    	
        if(!count($id_list)) return false;
        $ids = implode(', ', $id_list);
        
        $order_values = $this->config->get('also_bought_selected');
        if(!empty($order_values)) {
        	$order_values = implode(',', unserialize($order_values));
        }else{
        	return false;
        }


        // Get list of orders with related products
        $query = sprintf('
SELECT
    o.order_id
FROM
    %1$sorder_product AS op
INNER JOIN
    `%1$sorder` AS o ON op.order_id = o.order_id
WHERE
    o.order_status_id IN (%3$s)
AND
    op.product_id IN (%2$s)
GROUP BY
    o.order_id
', DB_PREFIX, $ids, $order_values);
        $res = $this->db->query($query);
        $orders = array();
        if($res->num_rows) {
            foreach($res->rows as $row) {
                $orders[] = $row['order_id'];
            }

            $query = sprintf('
SELECT
    op.product_id AS pid,
    COUNT(op.product_id) AS cnt
FROM
    %1$sorder_product AS op
WHERE
	op.order_id IN (%3$s)
AND
	op.product_id NOT IN (%2$s)
GROUP BY
    op.product_id
ORDER BY
    cnt
DESC LIMIT
    %4$s
', DB_PREFIX, $ids, implode(',', $orders), $limit);
            $res = $this->db->query($query);
            if($res->num_rows) return $res->rows;
        }
        unset($res);
        return false;
    }

    public function getItems($products, $limit) {
        $prods = $this->products($products, $limit);
        if(!$prods) return false;

        $product_list = array();

        foreach($prods as $prod) {
            $product_list[] = $prod['pid'];
        }
        
		return $product_list;


    }
}