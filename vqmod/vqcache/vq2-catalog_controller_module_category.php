<?php  

class ControllerModuleCategory extends Controller {

	protected function index($setting) {

		$this->language->load('module/category');

		$this->load->model('tool/image'); 

    	$this->data['heading_title'] = $this->language->get('heading_title');

		

		if (isset($this->request->get['path'])) {

			$parts = explode('_', (string)$this->request->get['path']);

		} else {

			$parts = array();

		}

		

		
			
			if (isset($parts[1])) {
			
		

			
			
			$this->data['category_id'] = $parts[1];
			
		

		} else {

			$this->data['category_id'] = 0;

		}

		

		
				
				if (isset($parts[2])) {
				
			

			
				
				$this->data['child_id'] = $parts[2];
				
			

		} else {

			$this->data['child_id'] = 0;

		}

							

		$this->load->model('catalog/category');



		$this->load->model('catalog/product');



		$this->data['categories'] = array();



		
				$categories = $this->model_catalog_category->getCategories_sidebar(0);
			

		foreach ($categories as $category) {

			
				
				$dat['filter_category_id'] = $category['category_id'];

				if(!$_REQUEST['car'])
				{
					$total = 1;
				}
				else
				{
					$dat['filter_sub_category'] = 1;
					$total = $this->model_catalog_product->getTotalProducts_with_car($dat,$_REQUEST['car']);
				}
				
				
			
			
			
			if($total)
			{

				if ($category['image']) {

					$this->data['thumb'] = $this->model_tool_image->resize($category['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));

				} else {

					$this->data['thumb'] = '';

				}

				$children_data = array();



				$children = $this->model_catalog_category->getCategories($category['category_id']);



				foreach ($children as $child) {

					$data = array(

						'filter_category_id'  => $child['category_id'],

						'filter_sub_category' => true

					);



					
				
				if(!$_REQUEST['car'])
				{
					$_REQUEST['car'] = "";
					$product_total = 1;
				}
				else
				{
					
					$data['filter_sub_category'] = 1;
					$product_total = $this->model_catalog_product->getTotalProducts_with_car($data,$_REQUEST['car']);
				}
				
				
			

					


					$total += $product_total;


					if($product_total)
					{
					
					$children_data[] = array(

						'category_id' => $child['category_id'],

						'name'        => $child['name'],

						
				'href'        => $this->url->link('product/category', 'path=' . $_REQUEST['car']. '_'.$category['category_id'] . '_' . $child['category_id'].'&car='.$_REQUEST['car'])	
				

					);		
					}

				}

			$this->data['categories'][] = array(

				'category_id' => $category['category_id'],

				'thumb'       => $category['image'],

				'name'        => $category['name'],

				'children'    => $children_data,

				'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])

			);	
			
			} //end of if no count

		}

		

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {

			$this->template = $this->config->get('config_template') . '/template/module/category.tpl';

		} else {

			$this->template = 'default/template/module/category.tpl';

		}

		

		$this->render();

  	}

}

?>