<?php 
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6    				
// Created by villagedefrance                        	  		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading 
$_['heading_title'] 			= 'Connexion Facebook';

// FB Text
$_['logging_text'] 				= 'Connexion en cours ...';
$_['canclled_text'] 			= 'Connexion avec Facebook annul&eacute;e.';

// Text
$_['text_greeting']			= 'Bienvenue, <strong>%s</strong> !';
$_['text_login']       			= 'Se Connecter';
$_['text_logout']      			= 'D&#233connexion';
$_['text_forgotten']   			= 'Mot de Passe oubli&#233;';
$_['text_register']    			= 'Cr&eacute;er un compte';

$_['text_account']       		= 'Compte';
$_['text_edit']          			= 'Editer le Compte';
$_['text_password']      		= 'Changer le Mot de Passe';
$_['text_address']       		= 'Modifier les Adresses';
$_['text_wishlist']      			= 'Modifier la Liste de Souhaits';
$_['text_order']         		= 'Historique de Commande';
$_['text_transaction']   		= 'Voir les Transactions';
$_['text_download']      		= 'T&eacute;l&eacute;chargements';
$_['text_addreturn']        	= 'Faire une Demande de Retour';
$_['text_return']        		= 'Voir les Retour de marchandises';
$_['text_reward']        		= 'Points de Fidelit&eacute;';
$_['text_giftvoucher'] 		= 'Acheter un Ch&#232;que-cadeau';
$_['text_newsletter']    		= 'Modifier la lettre d&#8217;information';

$_['text_code']       			= 'ID Client:';
$_['text_credit']       			= 'Balance:';
$_['text_points']				= 'Points gagn&eacute;s: %s';

// Entry
$_['entry_email']				= 'Adresse Courriel:';
$_['entry_password']			= 'Mot de Passe:';

// Buttons
$_['button_logout']		 	= 'D&#233connexion';
$_['button_login']		 		= 'Connexion';
?>