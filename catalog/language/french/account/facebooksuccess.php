<?php 
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6    				
// Created by villagedefrance                        	  		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading
$_['heading_title'] 		= 'Votre compte a &eacute;t&eacute; cr&eacute;&eacute; !';

// Text
$_['text_message']  		= '<p>F&eacute;licitations ! Votre nouveau compte a &eacute;t&eacute; cr&eacute;&eacute; avec succ&egrave;s en utilisant Facebook !</p> <p><h5>Informations importantes de compte sont requises !</h5>Veuillez v&eacute;rifier vos d&eacute;tails de compte et fournir une <b>Adresse de Livraison</b> valide, ainsi qu&#8217;un <b>Num&eacute;ro de T&eacute;l&eacute;phone</b> avant de placer votre premi&egrave;re commande. Cliquez sur <b>"Editer mon Compte"</b> en bas de page pour acc&eacute;der &agrave; votre compte.</p> <p>Si vous avez des questions sur le fonctionnement de notre boutique en ligne, vous pouvez nous contacter par courriel.</p><p>Une confirmation a &eacute;t&eacute; envoy&eacute; &agrave; l&#8217;adresse courriel fournie. Si vous ne l&#8217;avez pas re&ccedil;u dans l&#8217;heure, veuillez <a href="%s">nous contacter</a>.</p>';
$_['text_approval'] 		= '<p>Merci pour l&#8217;enregistrement chez %s !</p><p>Vous serez inform&eacute; par courriel d&egrave;s que votre compte sera activ&eacute;.</p><p>Veuillez bien noter qu&#8217;une <b>Adresse de Livraison</b> valide et un <b>Num&eacute;ro de T&eacute;l&eacute;phone</b> seront requis avant de placer votre premi&egrave;re commande.</p><p>Si vous avez des questions sur le fonctionnement de notre boutique en ligne, n&#8217;h&eacute;sitez pas &agrave; <a href="%s">nous contacter</a>.</p>';
$_['text_account']  		= 'Compte';
$_['text_success']  		= 'Succ&egrave;s';

// Button
$_['button_editaccount']	= 'Editer Mon Compte';
$_['button_continue']  	= 'Continuer';
?>