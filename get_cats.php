<?php
// Version
define('VERSION', '1.5.6');

/* Testing non-beta */

// Configuration
if (file_exists('config.php')) {
	require_once('config.php');
}  

// Install 
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

// VirtualQMOD
require_once('./vqmod/vqmod.php');
$vqmod = new VQMod();

// VQMODDED Startup
require_once($vqmod->modCheck(DIR_SYSTEM . 'startup.php'));

// Application Classes
require_once($vqmod->modCheck(DIR_SYSTEM . 'library/customer.php'));
require_once($vqmod->modCheck(DIR_SYSTEM . 'library/affiliate.php'));
require_once($vqmod->modCheck(DIR_SYSTEM . 'library/currency.php'));
require_once($vqmod->modCheck(DIR_SYSTEM . 'library/tax.php'));
require_once($vqmod->modCheck(DIR_SYSTEM . 'library/weight.php'));
require_once($vqmod->modCheck(DIR_SYSTEM . 'library/length.php'));
require_once($vqmod->modCheck(DIR_SYSTEM . 'library/cart.php'));

// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Config
$config = new Config();
$registry->set('config', $config);

// Database 
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);

// Store
if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
	$store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
} else {
	$store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
}

if ($store_query->num_rows) {
	$config->set('config_store_id', $store_query->row['store_id']);
} else {
	$config->set('config_store_id', 0);
}
		
// Settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0' OR store_id = '" . (int)$config->get('config_store_id') . "' ORDER BY store_id ASC");

foreach ($query->rows as $setting) {
	if (!$setting['serialized']) {
		$config->set($setting['key'], $setting['value']);
	} else {
		$config->set($setting['key'], unserialize($setting['value']));
	}
}

if (!$store_query->num_rows) {
	$config->set('config_url', HTTP_SERVER);
	$config->set('config_ssl', HTTPS_SERVER);	
}

// Url
$url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));	
$registry->set('url', $url);

// Log 
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);

//get the cat info
$parent_id = (int)$_REQUEST['id'];

if($_REQUEST['id']=="no")
	exit();

if($parent_id =="9775"||$parent_id =="9400"||$parent_id =="791")
	$sort_order = "c.sort_order, LCASE(cd.name) DESC";
else
	$sort_order = "c.sort_order, LCASE(cd.name)";

$query = $db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '1' AND c2s.store_id = '0'  AND c.status = '1' ORDER BY ".$sort_order);

$categories = $query->rows;

	$first_select_data = "";
	$first_select = "";
	$option_choice = "";
	foreach ($categories as $category) {
			// Level 1
			$first_select_data[] = array(
				'name'     => $category['name'],
				'id'   => $category['category_id']
			);
	}
	foreach($first_select_data as $data)
	{
		//put secial first
		if(substr($data['name'],0,1)=="*")
		{
			if($data['name']=="*Base") //for merc
			{
		$first_select .= "<option value=\"{$data['id']}\">".substr($data['name'],1)."</option>
";	
			}
			else
			{
			$option_choice =  "<option value=\"no\" class=\"select_bold\">-- ".substr($data['name'],1)." --</option>
";
			}
		}
		else
		{
		$first_select .= "<option value=\"{$data['id']}\">{$data['name']}</option>
";	
		}
	}
	
	if(isset($first_select))
	{
		$data['total'] = count($first_select_data);
		$data['id'] = $category['category_id'];
		$data['html'] = $option_choice.$first_select;
		echo json_encode($data);
	}

?>