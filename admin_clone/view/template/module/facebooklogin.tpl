<?php
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6    				
// Created by villagedefrance                        	  		
// contact@villagedefrance.net                         		
//-----------------------------------------------------
?>

<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>

	<div class="box">
	<div class="heading">
		<h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
		<div class="buttons">
			<a onclick="buttonSave();" class="button"><?php echo $button_save; ?></a>
			<a onclick="buttonApply();" class="button"><?php echo $button_apply; ?></a>
			<a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
		</div>
	</div>
	<div class="content">
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="fbloginModule">
		<div id="tabs" class="htabs">
			<a href="#tab-1"><span><?php echo $text_tabmain; ?></span></a>
			<a href="#tab-2"><span><?php echo $text_tabaccount; ?></span></a>
			<a href="#tab-3"><span><?php echo $text_tabfacebook; ?></span></a>
		</div>
		<div id="tab-1" style="clear: both;">
			<table class="form">
			<?php foreach ($languages as $language) { ?>
				<tr> 
					<td><?php echo $entry_title; ?></td> 
					<td> 
					<input type="text" name="facebooklogin_title<?php echo $language['language_id']; ?>" id="facebooklogin_title<?php echo $language['language_id']; ?>" size="30" value="<?php echo ${'facebooklogin_title' . $language['language_id']}; ?>" />
					<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
					</td>
				</tr>
			<?php } ?> 
				<tr> 
					<td><?php echo $entry_header; ?></td> 
					<td>
						<?php if ($facebooklogin_header) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_header" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_header" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_header" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_header" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr> 
					<td><?php echo $entry_icon; ?></td> 
					<td>
						<?php if ($facebooklogin_icon) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_icon" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_icon" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_icon" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_icon" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr> 
					<td><?php echo $entry_box; ?></td> 
					<td>
						<?php if ($facebooklogin_box) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_box" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_box" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_box" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_box" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
			</table>
		</div><!--tab1-->
	 
		<div id="tab-2" style="clear: both;">
			<table class="form">
			<h2><?php echo $text_accountopt; ?></h2>
				<tr>
					<td><?php echo $entry_wishlist; ?></td>
					<td>
						<?php if ($facebooklogin_wishlist) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_wishlist" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_wishlist" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_wishlist" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_wishlist" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td><?php echo $entry_download; ?></td>
					<td>
						<?php if ($facebooklogin_download) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_download" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_download" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_download" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_download" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td><?php echo $entry_reward; ?></td>
					<td>
						<?php if ($facebooklogin_reward) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_reward" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_reward" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_reward" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_reward" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td><?php echo $entry_voucher; ?></td>
					<td>
						<?php if ($facebooklogin_voucher) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_voucher" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_voucher" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_voucher" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_voucher" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td><?php echo $entry_letter; ?></td>
					<td>
						<?php if ($facebooklogin_letter) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_letter" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_letter" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_letter" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_letter" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr style="background:#FCFCFC;">
					<td><?php echo $entry_number; ?></td>
					<td>
						<?php if ($facebooklogin_number) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_number" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_number" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_number" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_number" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr style="background:#FCFCFC;">
					<td><?php echo $entry_balance; ?></td>
					<td>
						<?php if ($facebooklogin_balance) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_balance" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_balance" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_balance" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_balance" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
				<tr style="background:#FCFCFC;">
					<td><?php echo $entry_points; ?></td>
					<td>
						<?php if ($facebooklogin_points) { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_points" value="1" checked="checked" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_points" value="0" />
						<?php } else { ?>
							<?php echo $text_yes; ?><input type="radio" name="facebooklogin_points" value="1" />
							<?php echo $text_no; ?><input type="radio" name="facebooklogin_points" value="0" checked="checked" />
						<?php } ?>
					</td>
				</tr>
			</table>
		</div><!--tab2-->
	 
		<div id="tab-3" style="clear: both;">
			<table class="form">
			<h2><?php echo $text_fbsettings; ?></h2>
				<tr>
					<td><?php echo $text_fblink_help; ?></td>
					<td><a onclick="window.open('https://developers.facebook.com/apps');" title="developers.facebook.com/apps"><img src="view/image/facebook.png" alt="" /></a></td>
				</tr>
				<tr style="background:#FAFAFA;">
					<td><?php echo $entry_fblogin; ?></td>
					<td><select name="facebooklogin_fblogin">
						<?php if (isset($facebooklogin_fblogin)) {$selected = "selected"; ?>
							<option value="enable" <?php if($facebooklogin_fblogin=='enable'){echo $selected;} ?>><?php echo $text_enable; ?></option>
							<option value="disable" <?php if($facebooklogin_fblogin=='disable'){echo $selected;} ?>><?php echo $text_disable; ?></option>
						<?php } else { ?>
							<option selected="selected"></option>
							<option value="enable"><?php echo $text_enable; ?></option>
							<option value="disable"><?php echo $text_disable; ?></option>
						<?php } ?>
						</select>
					</td>
				</tr>
				<tr style="background:#FAFAFA;">
					<td><?php echo $entry_apikey; ?></td>
					<td><input type="text" name="facebooklogin_apikey" id="facebooklogin_apikey" size="40" value="<?php echo $facebooklogin_apikey; ?>" /></td>
				</tr>
				<tr style="background:#FAFAFA;">
					<td><?php echo $entry_apisecret; ?></td>
					<td><input type="text" name="facebooklogin_apisecret" id="facebooklogin_apisecret" size="50" value="<?php echo $facebooklogin_apisecret; ?>" /></td>
				</tr>
				<tr style="background:#FAFAFA;">
					<td><?php echo $entry_pwdsecret; ?></td>
					<td><input type="text" name="facebooklogin_pwdsecret" id="facebooklogin_pwdsecret" size="10" maxlength="8" value="<?php echo $facebooklogin_pwdsecret; ?>" />&nbsp;&nbsp;
						<span class="help"><?php echo $text_pwdsecret_help; ?></span>
					</td>
				</tr>
			<?php foreach ($languages as $language) { ?>
				<tr style="background:#FAFAFA;">
					<td><?php echo $entry_button; ?></td>
					<td><input type="text" name="facebooklogin_button_<?php echo $language['language_id']; ?>" id="facebooklogin_button_<?php echo $language['language_id']; ?>" size="40" value="<?php echo ${'facebooklogin_button_' . $language['language_id']}; ?>" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" />
					</td>
				</tr>
			<?php } ?>
			</table>
		</div><!--tab3-->
	
		<div class="versioncheck">
			<table class="form">
				<tr>
					<td></td>
					<td colspan="2"><?php echo $module_name; ?><b><?php echo $module_current_name; ?></b></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2"><?php echo $module_list; ?>
					<?php foreach ($compatibles as $compatible) { ?>
						<?php if($store_base_version == $compatible['opencart']) { ?>
							<b><?php echo $compatible['title']; ?></b>
						<?php } else { ?>
							<?php echo $compatible['title']; ?>
						<?php } ?>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2"><?php echo $store_version; ?>
					&nbsp;&nbsp;&nbsp;
					<?php foreach ($compatibles as $compatible) { ?>
						<?php if($store_base_version == $compatible['opencart']) { ?>
							<img src="view/image/success.png" alt="" />
						<?php } ?>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2"><?php echo $text_template; ?>
						<?php foreach ($templates as $template) { ?>
							<?php if ($template == $config_template) { ?>
								<b><?php echo $template; ?></b>
							<?php } ?>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2"><span style='color: #FF8800;'><b><?php echo $text_status; ?></b></span></td>
				</tr>
			<?php if($version && $revision) { ?>
				<tr>
					<td></td>
					<td><?php echo $module_current_version; ?></td>
					<td><?php echo $version; ?></td>
				</tr>
				<tr>
					<td></td>
					<td><?php echo $module_current_revision; ?></td>
					<td><?php echo $revision; ?></td>
				</tr>
			<?php } else { ?>
				<tr>
					<td></td>
					<td colspan="2"><?php echo $text_no_file; ?></td>
				</tr>
			<?php } ?>
			<?php if($ver_update || $rev_update) { ?>
				<tr>
					<td></td>
					<td colspan="2"><span style='color: #FF8800;'><b><?php echo $text_update; ?></b></span></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
					<?php echo $text_getupdate; ?>
					<br /><br />
					<a onclick="window.open('https://villagedefrance.net/index.php?route=account/login');" title="Villagedefrance"><img src="view/image/villagedefrance-30.png" alt="Villagedefrance" /></a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a onclick="window.open('https://www.opencart.com/index.php?route=account/login');" title="Opencart"><img src="view/image/opencart-30.png" alt="Opencart" /></a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a onclick="window.open('http://opencart-france.fr/index.php?route=account/login');" title="Opencart France"><img src="view/image/opencart-france-30.png" alt="Opencart France" /></a>
					<br />
					</td>
				</tr>
			<?php } ?>
				<tr>
					<td></td>
					<td colspan="2">
						<a onclick="window.open('http://villagedefrance.net/index.php?route=information/contact');" title="Contact Support" class="button"><?php echo $button_support; ?></a>
					</td>
				</tr>
			</table>
		</div>
		
		<table class="form">
			<tr>
				<td><a onclick="window.open('http://www.villagedefrance.net');" title="villagedefrance"><img src="view/image/villagedefrance.png" alt="" /></a></td>
				<td><b><?php echo $text_module_settings; ?></b></td>
			</tr>
		</table>
	
		<table id="module" class="list">
		<thead>
			<tr>
				<td class="left"><?php echo $entry_layout; ?></td>
				<td class="left"><?php echo $entry_position; ?></td>
				<td class="left"><?php echo $entry_status; ?></td>
				<td class="left"><?php echo $entry_sort_order; ?></td>
				<td></td>
			</tr>
		</thead>
		<?php $module_row = 0; ?>
		<?php foreach ($modules as $module) { ?>
		<tbody id="module-row<?php echo $module_row; ?>">
			<tr>
				<td class="left"><select name="facebooklogin_module[<?php echo $module_row; ?>][layout_id]">
				<?php foreach ($layouts as $layout) { ?>
				<?php if ($layout['layout_id'] == $module['layout_id']) { ?>
					<option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
				<?php } else { ?>
					<option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
				<?php } ?>
				<?php } ?>
				</select></td>
				<td class="left"><select name="facebooklogin_module[<?php echo $module_row; ?>][position]">
				<?php if ($module['position'] == 'content_top') { ?>
					<option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
				<?php } else { ?>
					<option value="content_top"><?php echo $text_content_top; ?></option>
				<?php } ?>
				<?php if ($module['position'] == 'content_bottom') { ?>
					<option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
				<?php } else { ?>
					<option value="content_bottom"><?php echo $text_content_bottom; ?></option>
				<?php } ?>
				<?php if ($module['position'] == 'column_left') { ?>
					<option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
				<?php } else { ?>
					<option value="column_left"><?php echo $text_column_left; ?></option>
				<?php } ?>
				<?php if ($module['position'] == 'column_right') { ?>
					<option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
				<?php } else { ?>
					<option value="column_right"><?php echo $text_column_right; ?></option>
				<?php } ?>
				</select></td>
				<td class="left"><select name="facebooklogin_module[<?php echo $module_row; ?>][status]">
				<?php if ($module['status']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
				</select></td>
				<td class="center"><input type="text" name="facebooklogin_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
				<td class="center"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>
			</tr>
		</tbody>
		<?php $module_row++; ?>
		<?php } ?>
		<tfoot>
			<tr>
				<td colspan="4"></td>
				<td class="center"><a onclick="addModule();" class="button"><span><?php echo $button_add_module; ?></span></a></td>
			</tr>
		</tfoot>
		</table>
		<input type="hidden" name="buttonForm" value="" />
	</form>
	</div>
		<br>
		<div style="text-align:center; color:#555;">Facebook Login v<?php echo $facebooklogin_version; ?></div>
	</div>
</div>

<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select name="facebooklogin_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="facebooklogin_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="facebooklogin_module[' + module_row + '][status]">';
	html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
	html += '      <option value="0"><?php echo $text_disabled; ?></option>';
	html += '    </select></td>';
	html += '    <td class="center"><input type="text" name="facebooklogin_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="center"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>

<script type="text/javascript"><!--
$(function() { $('#tabs a').tabs(); });
//--></script>

<script type="text/javascript"><!--
function buttonSave() {document.fbloginModule.buttonForm.value='save';$('#form').submit();}
function buttonApply() {document.fbloginModule.buttonForm.value='apply';$('#form').submit();}
//--></script>

<script type="text/javascript"><!--
$(document).ready(function(){	
	$('.versioncheck').hide().before('<a href="#" id="<?php echo 'versioncheck'; ?>" class="button" style="margin-bottom:10px;"><span><?php echo $button_showhide; ?></span></a>');
	$('a#<?php echo 'versioncheck'; ?>').click(function() {
		$('.versioncheck').slideToggle(1000);
		return false;
	});
});
//--></script>

<?php echo $footer; ?>