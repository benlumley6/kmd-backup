<?php
// Heading
$_['heading_title']           = 'Löschen';

// Text
$_['text_module']             = 'Modul';
$_['text_warning']            = 'Warnung: Der Inhalt der selektierten Tabellen wird unwiederbringlich GELÖSCHT! Bitte erstellen Sie eine Sicherung';
$_['text_success']            = 'Erfolgreich: Tabelleninhalte erfolgreich gelöscht!';

// Entry
$_['entry_models']            = 'Modelle auswählen:';
$_['entry_mode']              = 'Modus:';
$_['entry_mode_pro']          = 'Profi';
$_['entry_mode_simple']       = 'Einfach';
$_['entry_tables']            = 'Tabellen auswählen:';

// Error
$_['error_permission']        = 'Warnung: Sie haben keine Berechtigung, um das Modul Löschen zu ändern!';

// Button
$_['button_purge']            = 'Löschen';
$_['button_cancel']           = 'Abbruch';
?>