<?php 
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6					  			
// Created by villagedefrance                        	  		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading 
$_['heading_title'] 			= 'Connexion Facebook';

// FB Text
$_['logging_text'] 				= 'Connexion en cours ...';
$_['canclled_text'] 			= 'Connexion avec Facebook annul&eacute;e.';

// Text
$_['text_greeting']			= 'Welcome back, <strong>%s</strong> !';
$_['text_login']       			= 'Login';
$_['text_logout']      			= 'Logout';
$_['text_forgotten']   			= 'Forgotten Password';
$_['text_register']    			= 'Register Account';

$_['text_account']       		= 'My Account';
$_['text_edit']          			= 'Edit Account Information';
$_['text_password']      		= 'Change Password';
$_['text_address']       		= 'Modify Address Book';
$_['text_wishlist']      			= 'Modify Wish List';
$_['text_order']         		= 'View Order History';
$_['text_transaction']   		= 'View Transactions'; 
$_['text_download']      		= 'View Downloads';
$_['text_addreturn']   		= 'Add a Return Request'; 
$_['text_return']        		= 'View Return Requests'; 
$_['text_reward']        		= 'View Reward Points';
$_['text_giftvoucher'] 		= 'Purchase Gift Vouchers';
$_['text_newsletter']    		= 'Modify Newsletter';

$_['text_code']       			= 'Customer ID:';
$_['text_credit']       			= 'Balance:';
$_['text_points']				= 'Points earned: %s';

// Entry
$_['entry_email']				= 'E-Mail Address:';
$_['entry_password']			= 'Password:';

// Buttons
$_['button_logout']			= 'Logout';
$_['button_login']		 		= 'Login';
?>