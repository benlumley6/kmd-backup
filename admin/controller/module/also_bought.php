<?php

class ControllerModuleAlsoBought extends Controller {
    private $error = array();

    public function index() {
    	$this->data = array_merge($this->data, $this->language->load('module/also_bought'));

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');
        $this->load->model('localisation/order_status');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
        	if(empty($this->request->post['also_bought_selected'])) $this->request->post['also_bought_selected'] = array();
        	$this->request->post['also_bought_selected'] = serialize(array_keys($this->request->post['also_bought_selected']));
            $this->model_setting_setting->editSetting('also_bought', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array('href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'), 'text' => $this->language->get('text_home'),
            'separator' => false);

        $this->data['breadcrumbs'][] = array('href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'), 'text' => $this->language->get('text_module'),
            'separator' => ' :: ');

        $this->data['breadcrumbs'][] = array('href' => $this->url->link('module/also_bought', 'token=' . $this->session->data['token'], 'SSL'), 'text' => $this->language->get('heading_title'),
            'separator' => ' :: ');

        $this->data['action'] = $this->url->link('module/also_bought', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $data = array(
			'sort'  => 'name',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 9999
		);

        $options = $this->model_localisation_order_status->getOrderStatuses($data);
        $this->data['statuses'] = array();
        foreach($options as $option) {
        	$this->data['statuses'][$option['order_status_id']] = $option['name'];
        }

        if (isset($this->request->post['also_bought_selected'])) {
            $this->data['also_bought_selected'] = $this->request->post['also_bought_selected'];
        } else {
            $this->data['also_bought_selected'] = $this->config->get('also_bought_selected');
            if(empty($this->data['also_bought_selected'])) {
            	$this->data['also_bought_selected'] = array();
            }else{
            	$this->data['also_bought_selected'] = unserialize($this->data['also_bought_selected']);
            }
        }

        if (isset($this->request->post['also_bought_status'])) {
            $this->data['also_bought_status'] = $this->request->post['also_bought_status'];
        } else {
            $this->data['also_bought_status'] = $this->config->get('also_bought_status');
        }
		
		$this->data['modules'] = array();
		
		if (isset($this->request->post['also_bought_module'])) {
			$this->data['modules'] = $this->request->post['also_bought_module'];
		} elseif ($this->config->get('also_bought_module')) { 
			$this->data['modules'] = $this->config->get('also_bought_module');
		}	
				
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'module/also_bought.tpl';
        $this->children = array(
			'common/header',
			'common/footer'
		);

        $this->response->setOutput($this->render(true), $this->config->get('config_compression'));
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'module/also_bought')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}