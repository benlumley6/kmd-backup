<div class="box categoryModule">

  <div class="box-heading">Shop by Category</div>

  <div class="box-content">

    <ul class="box-category">

       <?php foreach ($categories as $category) { ?>

       <?php if($category['children']) { ?>

        <li class="has-child">

        <?php if ($category['category_id'] == $category_id) { ?>

        <a><?php echo $category['name']; ?></a>

        <?php } else { ?>

        <a><?php echo $category['name']; ?></a>

        <?php } ?>

      <?php } else { ?>

        <li>

        <?php if ($category['category_id'] == $category_id) { ?>

        <a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>

        <?php } else { ?>

        <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>

        <?php } ?>

      <?php } ?>

        

        <?php if ($category['children']) { ?>

        <ul class="sub-category">

          <?php foreach ($category['children'] as $child) { ?>

          <li>

            <?php if ($child['category_id'] == $child_id) { ?>

            <a href="<?php echo $child['href']; ?>" class="active"> - <?php echo $child['name']; ?></a>

            <?php } else { ?>

            <a href="<?php echo $child['href']; ?>"> - <?php echo $child['name']; ?></a>

            <?php } ?>

          </li>

          <?php } ?>

        </ul>

        <?php } ?>

      </li>

      <?php } ?>


    </ul>

  </div>

</div>

<script>
$(function(){
  $('.has-child').click(function() {
    $('.sub-category', this).toggle();
  });
});
</script>