<div class="box shopBrand">

  <div class="box-heading"><span><?php echo $heading_title; ?></span></div>

  <div class="box-content">

    <div class="box-category">

        <select onchange="window.open(this.options[this.selectedIndex].value,'_top')">

        <option>Pick a Brand</option>

        <?php foreach ($manufacturers as $manufacturer) { ?>

        <option value="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></option>

        <?php } ?>

      </select>

    </div>

  </div>

</div>