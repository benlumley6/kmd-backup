<?php
// Heading
$_['heading_title']			= 'Also Bought v1.5';

// Text
$_['text_module']			= 'Modules';
$_['text_success']			= 'Success: You have modified module also bought!';
$_['text_content_top']		= 'Content Top';
$_['text_content_bottom']	= 'Content Bottom';
$_['text_column_left']		= 'Column Left';
$_['text_column_right']		= 'Column Right';

// Entry
$_['entry_position']		= 'Position:';
$_['entry_selected']		= 'Select order statuses to use for also bought items:';
$_['entry_status']			= 'Status:';
$_['entry_layout']			= 'Layout:';
$_['entry_sort_order']		= 'Sort Order:';
$_['entry_show_basket']		= 'Show on basket page:';
$_['entry_show_product']	= 'Show on product page:';
$_['entry_limit']			= 'Item limit:';
$_['entry_image']			= 'Image (W x H):';

// Tabs
$_['tab_layout']			= 'Layout';
$_['tab_general']			= 'General';

// Error
$_['error_permission']		= 'Warning: You do not have permission to modify module also bought!';

$_['copyright']				= '&copy; Copyright <a style="text-decoration: none;" href="http://www.jaygilford.com/" target="_blank">Jay Gilford</a> 2010';
$_['store']					= '<strong>Find more modules <a style="text-decoration: none;" href="http://store.jaygilford.com/" target="_blank">at my store</a></strong>';