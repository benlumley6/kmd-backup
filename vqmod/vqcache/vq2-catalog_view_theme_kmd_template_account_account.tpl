<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php echo $column_left; ?><div class="is-column-fix"><?php echo $column_right; ?></div>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="pageHeading"><?php echo $heading_title; ?></h1>
  <h2><?php echo $text_my_account; ?></h2>
  <div class="content">
    
				<ul class="listLoop">
			
      
				<li><a href="<?php echo $edit; ?>"><img src="image/edit.jpg" alt="Edit your Account"><?php echo $text_edit; ?></a></li>
			
      
				<li><a href="<?php echo $password; ?>"><img src="image/password.jpg" alt="Change your Password"><?php echo $text_password; ?></a></li>
			
      
				<li><a href="<?php echo $address; ?>"><img src="image/address.jpg" alt="Change your address"><?php echo $text_address; ?></a></li>
			
      
				<li><a href="<?php echo $wishlist; ?>"><img src="image/wishlist.jpg" alt="Wishlist"><?php echo $text_wishlist; ?></a></li>
			
    </ul>
  </div>
  <h2><?php echo $text_my_orders; ?></h2>
  <div class="content">
    
				<ul class="listLoop">
			
      
				<li><a href="<?php echo $order; ?>"><img src="image/track.jpg" alt="View your History"><?php echo $text_order; ?></a></li>
			
      
				<li><a href="<?php echo $download; ?>"><img src="image/download.jpg" alt="View your Downloads"><?php echo $text_download; ?></a></li>
			
      <?php if ($reward) { ?>
      
				<li><a href="<?php echo $reward; ?>"><img src="image/rewards.jpg" alt="View your Reward Points"><?php echo $text_reward; ?></a></li>
			
      <?php } ?>
      
				<li><a href="<?php echo $return; ?>"><img src="image/returns.jpg" alt="View your Return Requests"><?php echo $text_return; ?></a></li>
			
      
				<li><a href="<?php echo $transaction; ?>"><img src="image/cash.jpg" alt="View your Transactions"><?php echo $text_transaction; ?></a></li>
			
    </ul>
  </div>
  <h2><?php echo $text_my_newsletter; ?></h2>
  <div class="content">
    
				<ul class="listLoop">
			
      
				<li><a href="<?php echo $newsletter; ?>"><img src="image/news.jpg" alt="Subscribe to our Newsletter"><?php echo $text_newsletter; ?></a></li>
			
    </ul>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 