<?php
// Heading
$_['heading_title']           = 'Purge';

// Text
$_['text_module']             = 'Modules';
$_['text_warning']            = 'Warning: The content of the selected tables will be permanently DELETED! Please create a backup!';
$_['text_success']            = 'Success: You have successfully deleted table contents!';

// Entry
$_['entry_tables']            = 'Select tables:';
$_['entry_models']            = 'Select models:';
$_['entry_mode']              = 'Mode:';
$_['entry_mode_simple']       = 'Simple';
$_['entry_mode_pro']          = 'Pro';

// Error 
$_['error_permission']        = 'Warning: You do not have permission to modify module purge!';

// Button
$_['button_purge']            = 'Purge';
$_['button_cancel']           = 'Cancel';
?>