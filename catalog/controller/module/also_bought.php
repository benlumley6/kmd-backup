<?php

class ControllerModuleAlsoBought extends Controller {
    protected $also_bought_id = 0;
    protected $path = array();

    protected function index($setting) {
    	$this->data = array_merge($this->data, $this->language->load('module/also_bought'));
        
        // Verify route is set and assign it to $r
        if(!isset($this->request->get['route'])) return;
        $r = $this->request->get['route'];
        
        
        if($r == 'product/product') {
            // Verify product id is set and legitimate
            if(!isset($this->request->get['product_id']) || !(int)$this->request->get['product_id']) {
                $this->log->write('Product ID not set on product page or invalid');
                return;
            }
            $products = array($this->request->get['product_id']);
        }else{
            if(!$this->cart->hasProducts()) return false;
            $temp = $this->cart->getProducts();
            $products = array();
            foreach($temp as $v) {
                $products[] = $v['product_id'];
            }
            $products = array_unique($products);
        }
        
        $this->load->model('tool/also_bought');
        $limit = $setting['limit'];
        if($limit < 3) {
            $limit = 3;
        }
        if($limit > 15) {
            $limit = 15;
        }
        
        $products = $this->model_tool_also_bought->getItems($products, $limit);
        if(!$products) return;
        
        $this->load->model('tool/image');
        $this->data['products'] = array();
        
		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', $setting['image_width'], $setting['image_height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
					
				$this->data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'thumb'   	 => $image,
					'name'    	 => $product_info['name'],
					'price'   	 => $price,
					'special' 	 => $special,
					'rating'     => $rating,
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
				);
			}
		}
		if(empty($this->data['products'])) return;
        
        $this->language->load('module/also_bought');
        $this->data['heading_title'] = $this->language->get('heading_title');


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/also_bought.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/also_bought.tpl';
        } else {
            $this->template = 'default/template/module/also_bought.tpl';
        }

        $this->render();
    }
}

?>