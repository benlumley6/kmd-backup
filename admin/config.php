<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/sites/kmd/admin/');
define('HTTP_CATALOG', 'http://localhost/sites/kmd/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/sites/kmd/admin/');
define('HTTPS_CATALOG', 'http://localhost/sites/kmd/');

// DIR
define('DIR_APPLICATION', 'C:\xampp\htdocs\sites\kmd/admin/');
define('DIR_SYSTEM', 'C:\xampp\htdocs\sites\kmd/system/');
define('DIR_DATABASE', 'C:\xampp\htdocs\sites\kmd/system/database/');
define('DIR_LANGUAGE', 'C:\xampp\htdocs\sites\kmd/admin/language/');
define('DIR_TEMPLATE', 'C:\xampp\htdocs\sites\kmd/admin/view/template/');
define('DIR_CONFIG', 'C:\xampp\htdocs\sites\kmd/system/config/');
define('DIR_IMAGE', 'C:\xampp\htdocs\sites\kmd/image/');
define('DIR_CACHE', 'C:\xampp\htdocs\sites\kmd/system/cache/');
define('DIR_DOWNLOAD', 'C:\xampp\htdocs\sites\kmd/download/');
define('DIR_LOGS', 'C:\xampp\htdocs\sites\kmd/system/logs/');
define('DIR_CATALOG', 'C:\xampp\htdocs\sites\kmd/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'master_newoc');
define('DB_PASSWORD', 'r2c6utyNu7xq');
define('DB_DATABASE', 'master_newoc');
define('DB_PREFIX', 'oc_');
?>