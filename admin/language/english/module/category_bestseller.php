<?php
// Heading
$_['heading_title']       = 'Category Bestsellers';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Category Bestsellers!';

// Entry
$_['entry_limit_shown']   = 'Limit shown:';
$_['entry_limit_hidden']  = 'Limit hidden:<br /><span class="help">Loaded after click on button.</span>';
$_['entry_image']         = 'Image (W x H):<br /><span class="help">Leave empty to disable images.</span>';
$_['entry_description']   = 'Product description length:<br /><span class="help">Leave empty or 0 to disable product description</span>';
$_['entry_sub_category']  = 'Search in subcategories:';
$_['entry_status']        = 'Status:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Category Bestsellers!';
?>