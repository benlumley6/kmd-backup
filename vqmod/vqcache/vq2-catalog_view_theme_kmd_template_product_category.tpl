<?php echo $header; ?>

<div class="breadcrumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>

    <?php } ?>

  </div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content"><?php echo $content_top; ?>

  <?php if ($category_bestseller_status && $category_bestseller) { // CATEGORY_BESTSELLER start ?>
  <div id="category_bestseller">
    <h2 class="box-heading"><?php echo $text_category_bestseller; ?></h2>
    <div id="category_bestseller-shown">
	<?php $i = 1; ?>
	<?php foreach ($category_bestseller as $item) { ?>
	  <div class="category_bestseller-item">
		<?php if ($item['image']) { ?>
		<div class="category_bestseller-image"><a href="<?php echo $item['href']; ?>"><img src="<?php echo $item['image']; ?>" alt="<?php echo $item['name']; ?>" style="width:<?php echo $item['width']; ?>px;height:<?php echo $item['height']; ?>px;" /></a></div>
		<?php } ?>
		<div class="category_bestseller-desc">
		  <span class="category_bestseller-top">
		    <a href="<?php echo $item['href']; ?>" class="category_bestseller-name"><?php echo $item['name']; ?></a>
			- <span class="category_bestseller-stock"><?php echo $item['stock']; ?></span>
		  </span>
		  <?php if ($item['description']) { ?>
		  <br /><span class="category_bestseller-bottom"><?php echo $item['description']; ?></span>
		  <?php } ?>
		</div>
		<?php if ($item['price']) { ?>
		<div class="category_bestseller-price">
		  <?php if ($item['special']) { ?>
		  <span class="price-old"><?php echo $item['price']; ?></span> <span class="price-new"><?php echo $item['special']; ?></span>
		  <?php } else { ?>
		  <?php echo $item['price']; ?>
		  <?php } ?>
		  <?php if ($item['tax']) { ?>
		  <br />
		  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $item['tax']; ?></span>
		  <?php } ?>
		</div>
		<?php } ?>

		<div class="cart">
        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $item['product_id']; ?>');" class="button" />
      </div>
	  </div>
	<?php } ?>
	</div>
	<div id="category_bestseller-hidden"></div>
	<div id="category_bestseller-button_holder">
	  <a id="category_bestseller-button"><?php echo $button_category_bestseller; ?></a>
	</div>
  </div>
  <?php } // CATEGORY_BESTSELLER end ?>
			

  <div class="box">

  <div class="box-heading"><?php echo $heading_title; ?></div>

  <?php /* if ($thumb || $description) { ?>

  <div class="category-info">

    <?php if ($thumb) { ?>

    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>

    <?php } ?>

    <?php if ($description) { ?>

    <?php echo $description; ?>

    <?php } ?>

  </div>

  <?php } */ ?>

  <?php if ($categories) { ?>

  <div class="category-list">

  <h2><?php echo $text_refine; ?></h2>

    <?php if (count($categories) <= 5) { ?>

    <ul>

      <?php foreach ($categories as $category) { ?>

      <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>

      <?php } ?>

    </ul>

    <?php } else { ?>

    <?php for ($i = 0; $i < count($categories);) { ?>

    <ul>

      <?php $j = $i + ceil(count($categories) / 4); ?>

      <?php for (; $i < $j; $i++) { ?>

      <?php if (isset($categories[$i])) { ?>

      <li><a href="<?php echo $categories[$i]['href']; ?>"><?php echo $categories[$i]['name']; ?></a></li>

      <?php } ?>

      <?php } ?>

    </ul>

    <?php } ?>

    <?php } ?>

  </div>

  <?php } ?>

  <?php if ($products) { ?>

  <?php /* <div class="product-filter">

    <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>

    <div class="limit"><b><?php echo $text_limit; ?></b>

      <select onchange="location = this.value;">

        <?php foreach ($limits as $limits) { ?>

        <?php if ($limits['value'] == $limit) { ?>

        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>

        <?php } else { ?>

        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>

        <?php } ?>

        <?php } ?>

      </select>

    </div>

    <div class="sort"><b><?php echo $text_sort; ?></b>

      <select onchange="location = this.value;">

        <?php foreach ($sorts as $sorts) { ?>

        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>

        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>

        <?php } else { ?>

        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>

        <?php } ?>

        <?php } ?>

      </select>

    </div>

  </div>

  <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div> */ ?>

  <div class="product-grid">

    <?php foreach ($products as $product) { ?>

    <div>

      <?php if ($product['thumb']) { ?>

      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>

      <?php } ?>

      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>

      <div class="description"><?php echo $product['description']; ?></div>

      <?php /* if ($product['price']) { ?>

      <div class="price">

        <?php if (!$product['special']) { ?>

        <?php echo $product['price']; ?>

        <?php } else { ?>

        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>

        <?php } ?>

        <?php if ($product['tax']) { ?>

        <br />

        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>

        <?php } ?>

      </div>

      <?php } */ ?>

      <div class="cart">

        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />

      </div>

    </div>

    <?php } ?>

    </div><!-- box -->

  </div>

  <div class="pagination"><?php echo $pagination; ?></div>

  <?php } ?>

  <?php if (!$categories && !$products) { ?>

  <div class="content"><?php echo $text_empty; ?></div>



  <?php } ?>

  <?php echo $content_bottom; ?></div>

<script type="text/javascript"><!--

function display(view) {

	if (view == 'list') {

		$('.product-grid').attr('class', 'product-list');

		

		$('.product-list > div').each(function(index, element) {

			html  = '<div class="right">';

			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';

			html += '</div>';			

			

			html += '<div class="left">';

			

			var image = $(element).find('.image').html();

			

			if (image != null) { 

				html += '<div class="image">' + image + '</div>';

			}

			

			var price = $(element).find('.price').html();

			

			if (price != null) {

				html += '<div class="price">' + price  + '</div>';

			}

					

			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';

			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';

			

			var rating = $(element).find('.rating').html();

			

			if (rating != null) {

				html += '<div class="rating">' + rating + '</div>';

			}

				

			html += '</div>';

						

			$(element).html(html);

		});		

		

		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');

		

		$.totalStorage('display', 'list'); 

	} else {

		$('.product-list').attr('class', 'product-grid');

		

		$('.product-grid > div').each(function(index, element) {

			html = '';

			

			var image = $(element).find('.image').html();

			

			if (image != null) {

				html += '<div class="image">' + image + '</div>';

			}

			

			html += '<div class="name">' + $(element).find('.name').html() + '</div>';

			html += '<div class="description">' + $(element).find('.description').html() + '</div>';

			

			var price = $(element).find('.price').html();

			

			if (price != null) {

				html += '<div class="price">' + price  + '</div>';

			}

			

			var rating = $(element).find('.rating').html();

			

			if (rating != null) {

				html += '<div class="rating">' + rating + '</div>';

			}

						

			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';

			

			$(element).html(html);

		});	

					

		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');

		

		$.totalStorage('display', 'grid');

	}

}



view = $.totalStorage('display');



if (view) {

	display(view);

} else {

	display('grid');

}

//--></script> 


<?php if ($category_bestseller_status && $category_bestseller) { // CATEGORY_BESTSELLER start ?>
<style>
#category_bestseller {
	margin-bottom: 30px;
	background: #f1eff0;
	border-bottom: 1px solid #aaa;
	border-left: 1px solid #aaa;
	border-right: 1px solid #aaa;
}
#category_bestseller-shown .cart {
	position: relative;
	top: 17px;
}
.category_bestseller-item {
	margin-bottom: 5px;
	width: 168px;
	display: inline-block;
	vertical-align: top;
	padding: 14px 7px;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-ms-box-sizing: border-box;
	background: #fff;
	margin: 19px 13px;
	height: 242px !important;
	text-align: center;
}
.category_bestseller-number {
	float: left;
	vertical-align: middle;
	text-align: center;
	font-size: 16px;
	background-color: #ECF3F6;
	width: 25px;
}
.category_bestseller-image {
	display: block;
	margin: 0px auto;
	width: 124px;
}
.category_bestseller-desc {
}
.category_bestseller-top a {
	color: #000;
	font-weight: bold;
	text-align: center;
	float: none;
	width: 100%;
	text-decoration: none;
}
.category_bestseller-bottom,
.category_bestseller-stock {
	display: none;
}
.category_bestseller-price {
	display: none
}
.category_bestseller-price .price-old {
	text-decoration: line-through;
	font-weight: normal;
	color: #000;
}
.category_bestseller-price .price-tax {
	color: #000;
	font-weight: normal;
	font-size: 0.8em;
}
#category_bestseller-button_holder {
	text-align: center;
	border-top: 1px solid #DDD;
	padding-top: 5px;
}
#category_bestseller-button {
	padding: 5px 10px;
	background-color: #EEE;
	border: 1px solid #DDD;
	text-decoration: none;
	color: #00275A;
}
</style>
<script type="text/javascript">
function categoryBestseller() {
	var min_w = 99999;

	$('.category_bestseller-item').each(function() {
		var w = $(this).width() - $('.category_bestseller-number', this).outerWidth(true);

		if ($('.category_bestseller-image', this)) {
			w -= $('.category_bestseller-image', this).outerWidth(true);
		}

		if ($('.category_bestseller-price', this)) {
			w -= $('.category_bestseller-price', this).outerWidth(true);
		}

		if (w < min_w) {
			min_w = w;
		}
	});

	$('.category_bestseller-item .category_bestseller-desc').css('width', min_w);

	var h = $('.category_bestseller-item:first .category_bestseller-image').outerHeight();

	$('.category_bestseller-item').css('height', h);
	$('.category_bestseller-item .category_bestseller-number').css('line-height', h + 'px');
}

categoryBestseller();

$('#category_bestseller-button').click(function() {
	if ($('#category_bestseller-hidden').html()) {
		if ($(this).hasClass('opened')) {
			$('#category_bestseller-hidden').slideUp(400);
			$(this).removeClass('opened');
		} else {
			$('#category_bestseller-hidden').slideDown(400);
			$(this).addClass('opened');
		}
	} else {
		$.ajax({
			url: 'index.php?route=product/category/categoryBestseller&category_id=<?php echo $category_id; ?>',
			dataType: 'json',
			beforeSend: function() {
				$('#category_bestseller-hidden').html('<div style="text-align:center;"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>');
			},
			success: function(data) {
				var html = '';

				var j = $('#category_bestseller-shown .category_bestseller-item').length + 1;

				for (var i in data.products) {
					html += '<div class="category_bestseller-item">';
					html += '  <div class="category_bestseller-number">' + (j++) + '</div>';

					if (data.products[i].image) {
						html += '  <div class="category_bestseller-image"><a href="' + data.products[i].href + '"><img src="' + data.products[i].image + '" alt="' + data.products[i].name + '" style="width:' + data.products[i].width + 'px;height:' + data.products[i].height + 'px;" /></a></div>';
					}

					html += '  <div class="category_bestseller-desc">';
					html += '    <span class="category_bestseller-top">';
					html += '      <a href="' + data.products[i].href + '" class="category_bestseller-name">' + data.products[i].name + '</a>';
					html += '      - <span class="category_bestseller-stock">' + data.products[i].stock + '</span>';
					html += '    </span>';

					if (data.products[i].description) {
						html += '    <br /><span class="category_bestseller-bottom">' + data.products[i].description + '</span>';
					}

					html += '  </div>';

					if (data.products[i].price) {
						html += '  <div class="category_bestseller-price">';

						if (data.products[i].special) {
							html += '    <span class="price-old">' + data.products[i].price + '</span> <span class="price-new">' + data.products[i].special + '</span>';
						} else {
							html += data.products[i].price;
						}

						if (data.products[i].tax) {
							html += '    <br />';
							html += '    <span class="price-tax"><?php echo $text_tax; ?> ' + data.products[i].tax + '</span>';
						}

						html += '  </div>';
					}

					html += '</div>';
				}

				$('#category_bestseller-hidden').slideUp(200, function() {
					$('#category_bestseller-hidden').html(html).slideDown(400, categoryBestseller);
					$('#category_bestseller-button').addClass('opened');
				});
			}
		});
	}
});
</script>
<?php } // CATEGORY_BESTSELLER end ?>
			
<?php echo $footer; ?>