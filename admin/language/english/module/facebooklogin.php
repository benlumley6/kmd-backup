<?php 
//-----------------------------------------------------
// Facebook Login for Opencart v1.5.6    				
// Created by villagedefrance                        	  		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading
$_['heading_title']       		= 'Facebook Login';

// Text
$_['text_module']         		= 'Modules';
$_['text_success']        		= 'Success: You have modified module <b>Facebook Login</b> !';
$_['text_yes']	       			= 'Yes'; 
$_['text_no']	       			= 'No';
$_['text_enable']	       		= 'Enable'; 
$_['text_disable']	       		= 'Disable';
$_['text_content_top']    		= 'Content Top';
$_['text_content_bottom'] 	= 'Content Bottom';
$_['text_column_left']    		= 'Column Left';
$_['text_column_right']   		= 'Column Right';
$_['text_fblink_help']			= 'Get Facebook API Key/API Secret:';
$_['text_pwdsecret_help']	= 'Adds extra encryption to passwords: 4 to 8 lowercase characters.';
$_['text_module_settings']	= 'Note: these settings will be applied to All the modules in the list below.';

// <H2>
$_['text_accountopt']   		= 'Customer Account Options';
$_['text_fbsettings']   		= 'Facebook Settings';

// Tabs
$_['text_tabmain']        		= 'General Options';
$_['text_tabaccount']        	= 'Account Options';
$_['text_tabfacebook']       	= 'Facebook Settings';

// Version Check
$_['text_module_name'] 		= 'Module Name : ';
$_['text_module_list']			= 'Module Compatibility : ';
$_['text_store_version'] 		= 'Opencart Version Detected : <strong>%s</strong>';
$_['text_template']			= 'Active Template : ';

$_['text_module_version'] 	= 'Module Version : <strong>%s</strong>';
$_['text_module_revision'] 	= 'Module Revision : <strong>%s</strong>';
$_['text_v_update'] 			= '<img src="view/image/warning.png" alt="" /> &nbsp; <strong>%s</strong> : An update is available !';
$_['text_v_no_update'] 		= '<img src="view/image/success.png" alt="" /> &nbsp; <strong>%s</strong> : You have the latest Version.';
$_['text_r_update'] 			= '<img src="view/image/warning.png" alt="" /> &nbsp; <strong>%s</strong> : An update is available !';
$_['text_r_no_update'] 		= '<img src="view/image/success.png" alt="" /> &nbsp; <strong>%s</strong> : You have the latest Revision.';
$_['text_no_revision'] 		= '<img src="view/image/warning.png" alt="" /> &nbsp; <b>Revision Data not available.</b> Please update your Version !';
$_['text_no_file'] 				= 'The Module Version Check File is not available from the remote server !';
$_['text_status'] 				= 'MODULE VERSION STATUS';
$_['text_update'] 				= 'MODULE UPDATE OPTIONS';
$_['text_getupdate'] 			= 'Click on one of the links below to login to your account and download the latest update.';

$_['button_showhide'] 		= 'Version Checker';
$_['button_support'] 			= 'Support';

$_['text_v153']				= 'v1.5.3, ';
$_['text_v154']				= 'v1.5.4, ';
$_['text_v155']				= 'v1.5.5, ';
$_['text_v156']				= 'v1.5.6.';

// Entry
$_['entry_title']      			= 'Custom Title:<br /><span class="help">If empty and "Show Header" is set to yes, variable from the language will be shown.</span>'; 
$_['entry_header'] 			= 'Show Header:<br /><span class="help">"Show Box" must be selected.</span>'; 
$_['entry_icon']   				= 'Show Icon:<br /><span class="help">"Show Box" must be selected.</span>'; 
$_['entry_box']   				= 'Show Box:'; 

$_['entry_wishlist']   			= 'Hide Wishlist Link:'; 
$_['entry_download']   		= 'Hide Download Link:'; 
$_['entry_reward']   			= 'Hide Reward Link:'; 
$_['entry_voucher']        		= 'Hide Voucher Link:';
$_['entry_letter']        		= 'Hide Newsletter Link:';

$_['entry_number']   			= 'Show Customer ID:<br /><span class="help">Display customer ID ?</span>'; 
$_['entry_balance']   			= 'Show Customer Balance:<br /><span class="help">Display customer credit balance ?</span>'; 
$_['entry_points']   			= 'Show Customer Points:<br /><span class="help">Display customer earned points ?</span>'; 

$_['entry_fblogin']        		= 'Use Facebook Login:';
$_['entry_apikey']        		= 'Facebook API Key:<br /><span class="help">Required if Facebook Login is enabled.</span>';
$_['entry_apisecret']     		= 'Facebook API Secret:<br /><span class="help">Required if Facebook Login is enabled.</span>';
$_['entry_pwdsecret']     		= 'Password Encryption Code:<br /><span class="help">Required if Facebook Login is enabled (e.g: x2y4z6).</span>';
$_['entry_button']	  			= 'Edit Facebook Button:';

$_['entry_layout']        		= 'Layout:';
$_['entry_position']      		= 'Position:';
$_['entry_status']        		= 'Status:';
$_['entry_sort_order']    		= 'Sort Order:';

// Buttons
$_['button_save']				= 'Save';
$_['button_apply']				= 'Apply';

// Error
$_['error_permission']    		= 'Warning: You do not have permission to modify module <b>Facebook Login</b> !';
?>