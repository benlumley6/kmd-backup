<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/category.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a href="<?php echo $repair; ?>" class="button"><?php echo $button_repair; ?></a><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('#form').submit();" class="button"><?php echo $button_delete; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php echo $column_name; ?></td>
              <td class="right"><?php echo $column_sort_order; ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($categories) { 
			$after_real = 0;
			$this_name = "";
			$col2 = "";
			?>
            <?php foreach ($categories as $category) { ?>
			
			<?php 
		
			$col = "";
			if(substr_count($category['name'],"&gt;")>5) 
			{
				$col= 'style="background-color:red;"';
				$after_real = 1;
				//get the position of the 3rd arrow
				$pos = strposOffset("&gt;",$category['name'], 3);
				$this_name = substr($category['name'],0,$pos);
				
				$end_pos = strrpos ($category['name'] , "&gt;" );
				$end_name = substr($category['name'],($end_pos+4));
				
				/*echo '<script>
						$("tr td:contains(\''.$end_name.'\')").each(function(){
						
					if($(this).css("background-color")=="rgb(255, 255, 255)")
					{
						$(this).css("background","green");
						//select the box
						var check = ($(this).parent().find("input"));
						check.prop("checked", true);
					}
			});
				</script>';
				*/
				
			}
			
			$pos = strposOffset("&gt;",$category['name'], 3);
			$current_name = substr($category['name'],0,$pos);
			
			if($current_name!=$this_name)
			{
				$after_real = 0; //reset the real
				$col2 = "";
			}
			
			
			
				?>
			<tr>
              <td style="text-align: center;"><?php //if ($category['selected'] || $after_real>1) { 
			  if ($category['selected']) {
			  ?>
                <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" />
                <?php } ?>											</td>
              <td <?php echo $col; echo $col2; ?> class="left"><?php echo $category['name']; ?></td>
              <td class="right"><?php echo $category['sort_order']; ?></td>
              <td class="right"><?php foreach ($category['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php 
			if($after_real)
			{
				$after_real++;
				$col2 = 'style="background-color:yellow;"';
			}
			
			} ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<?php echo $footer; 

function strposOffset($search, $string, $offset)
{
    /*** explode the string ***/
    $arr = explode($search, $string);
    /*** check the search is not out of bounds ***/
    switch( $offset )
    {
        case $offset == 0:
        return false;
        break;
    
        case $offset > max(array_keys($arr)):
        return false;
        break;

        default:
        return strlen(implode($search, array_slice($arr, 0, $offset)));
    }
}
?>