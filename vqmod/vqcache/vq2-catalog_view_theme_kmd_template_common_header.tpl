<!DOCTYPE html>

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<head>

<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<meta charset="UTF-8" />

<title><?php echo $title; ?></title>

<base href="<?php echo $base; ?>" />

<?php if ($description) { ?>

<meta name="description" content="<?php echo $description; ?>" />

<?php } ?>

<?php if ($keywords) { ?>

<meta name="keywords" content="<?php echo $keywords; ?>" />

<?php } ?>

<?php if ($icon) { ?>

<link href="<?php echo $icon; ?>" rel="icon" />

<?php } ?>

<?php foreach ($links as $link) { ?>

<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />

<?php } ?>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/kmd/stylesheet/stylesheet.css" />

				<link rel="stylesheet" type="text/css" href="catalog/view/theme/kmd/stylesheet/dijitul_acct_page.css" />
			

<?php foreach ($styles as $style) { ?>

<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />

<?php } ?>

<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>

<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>

<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />

<script type="text/javascript" src="catalog/view/javascript/common.js"></script>

<?php foreach ($scripts as $script) { ?>

<script type="text/javascript" src="<?php echo $script; ?>"></script>

<?php } ?>

<!--[if IE 9]> 

<link rel="stylesheet" type="text/css" href="catalog/view/theme/kmd/stylesheet/ie9.css" />

<![endif]-->

<!--[if IE 8]> 

<link rel="stylesheet" type="text/css" href="catalog/view/theme/kmd/stylesheet/ie8.css" />

<![endif]-->

<!--[if IE 7]> 

<link rel="stylesheet" type="text/css" href="catalog/view/theme/kmd/stylesheet/ie7.css" />

<![endif]-->

<!--[if lt IE 7]>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />

<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>

<script type="text/javascript">

DD_belatedPNG.fix('#logo img');

</script>

<![endif]-->

<!-- toggle for drop down car menu -->

<style>
.menuDropdown
{
	padding-bottom:1000px;
}
</style>

<script type="text/javascript">
  $(document).ready(function() {

    // Choose your vehicle dropdrown
    $('.menuTab').mouseenter(function() {
      $('.menuDropdown').show();
    });
	
	//$('.dropLeft select').mouseenter(function() {
	 //    event.stopPropagation();
    //});
	//$('.dropLeft select').mouseleave(function() {
	 //    event.stopPropagation();
    //});
	
	//$('.m_body').click(function() {
//      $('.menuDropdown').hide();
  //  });
	

   

    // http://jsfiddle.net/mPDcu/1/ 


    // Hover states for category module
    $( ".box-category li" ).click(function() {
          $(this).find('.child_category li').toggle();
      });

  });
</script>

<?php if ($stores) { ?>

<script type="text/javascript"><!--

$(document).ready(function() {

<?php foreach ($stores as $store) { ?>

$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');

<?php } ?>

});

//--></script>

<?php } ?>

<?php echo $google_analytics; ?>

</head>

<body<?php 
					if(isset($this->request->get['route'])){
						$class 	 = explode('/',$this->request->get['route']);
						$id 	 = 'p'.$class[1];
						$class 	 = 'p'.implode(' ',array_splice($class,1));
						$class 	.= ' class-'.str_replace('/','-',$this->request->get['route']);
						if(isset($this->request->get['path'])){
							$this->load->model('catalog/category');
							$cats  	= explode('_',$this->request->get['path']);
							$cats 	= !is_array($cats)? array($cats) : $cats;
							foreach($cats as $cat){
								$model 	= $this->model_catalog_category->getCategory($cat);
								$class .= ' class-'.str_replace(' ','-',preg_replace('/[^a-z0-9\s]/','',strtolower($model['name'])));
							}
						}
					}else{
						$class 	 = 'home common-home';
						$id 	 = 'phome';
					}
					echo ' id="'.$id.'" class="'.$class.'" ';
			?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fullwidth topWrap">

<div class="body">

<div id="header">

  <?php if ($logo) { ?>

  <div id="logo"><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>

  <?php } ?>

  <div class="headerBanner">
    <?php echo $content_header ?>
  </div><!-- headerBanner -->

  <div class="social">

    <a href="https://twitter.com/KMDTuning" title="Twitter" class="twitter" target="_blank"><img src="image/twitter.png" alt="twitter"></a>
    <a href="https://www.youtube.com/channel/UCC-zlN3FF1lZ8Uj0g5h6Eaw/videos" title="Youtube" class="youtube" target="_blank"><img src="image/youtube.png" alt="youtube"></a>
    <a href="https://www.facebook.com/kmdtuning" title="Facebook" class="facebook" target="_blank"><img src="image/facebook.png" alt="facebook"></a>
    <a href="https://www.flickr.com/photos/87946320@N04" title="Flickr" class="flickr" target="_blank"><img src="image/flickr.png" alt="flickr"></a>
    <a href="http://instagram.com/kmdtuning" title="Instagram" class=2"instagram" target="_blank"><img src="image/instagram.png" alt="instagram"></a>
    <a href="" title="Live Help" class="livehelp"><img src="image/live_help.png" alt="livehelp"></a>
    
  </div><!-- social -->

  <div id="search">

    <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />

  </div><!-- search -->

</div><!-- header -->

</div><!-- body -->

</div><!-- fullwidth -->

<div class="fullwidth navigation">

<div class="body">

  <div class="menuTab">
    <p class="tabHeading">
      Choose your<br >
      <span>Vehicle</span>
    </p><!-- tabHeading -->
    <img src="image/car_tab.png" alt="Kmd Tuning">
  </div><!-- menuTab -->

  <div class="menuDropdown">

  <div class="menuDrop-inner">

    <div class="dropLeft">
	  <select name="make" class="dropdown_ajax" id="car_make">
		<option value="no" class="select_bold">-- Make --</option>
		<?php echo $first_select; ?>
	  </select>
	  <span class="dropSelects"></span> <!-- placeholder for ajax -->
      <input type="checkbox" id="rememberCar">
      <label for="rememberCar">Remember this vehicle</label>

    </div><!-- dropLeft -->

    <div class="dropRight">
      
        <p>Some text about this feature can go here</p>

        <a class="goButton" title="Let's get Started"></a>

    </div><!-- dropRight -->

    </div><!-- inner -->

  </div><!-- menuDropdown -->

<div id="menu">

  <?php /* <ul>

    <?php foreach ($categories as $category) { ?>

    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>

      <?php if ($category['children']) { ?>

      <div>

        <?php for ($i = 0; $i < count($category['children']);) { ?>

        <ul>

          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>

          <?php for (; $i < $j; $i++) { ?>

          <?php if (isset($category['children'][$i])) { ?>

          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>

          <?php } ?>

          <?php } ?>

        </ul>

        <?php } ?>

      </div>

      <?php } ?>

    </li>

    <?php } ?>

  </ul> */ ?>

  <div class="topMenu">

  <div class="links"><a href="<?php echo $home; ?>" class="first"><?php echo $text_home; ?></a><a href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a><a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>

  </div><!-- links -->

  <?php echo $cart; ?>

  </div><!-- topMenu -->


</div>

</div><!-- body -->

</div><!-- fullwidth -->

<?php
if(!isset($this->request->get['route']))
  $this->request->get['route'] = "";

 if($this->request->get['route'] == 'common/home' || $_SERVER['REQUEST_URI'] == '/') { 

 ?>
    <div class="contentBg">
      <div class="body m_body">

        <img src="image/content_bg.png" alt="contentbg">

      </div><!-- body -->
    </div><!-- content_bg -->

<?php } else { ?>

<?php } // end if ?>

<div class="body m_body">

<?php if ($error) { ?>

    

    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>

    

<?php } ?>



<div id="notification"></div>

